-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th4 18, 2022 lúc 05:48 PM
-- Phiên bản máy phục vụ: 10.4.20-MariaDB
-- Phiên bản PHP: 7.3.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `banbds`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2022_03_16_050416_create_tbl_admin_table', 1),
(6, '2022_03_18_033448_create_tbl_danhmuc_nhadat', 2),
(7, '2022_03_18_095340_create_tbl_du_an', 3),
(8, '2022_03_18_172251_create_tbl_khu_vuc', 4),
(9, '2022_03_18_182336_create_tbl_bat_dong_san', 4),
(10, '2022_03_30_144325_tbl_khach_hang', 5),
(11, '2022_03_30_164215_tbl_thong_tin_dat_coc', 6),
(14, '2022_04_04_161840_tbl_dat_coc', 7),
(15, '2022_04_04_164910_tbl_don_hang', 7),
(16, '2022_04_04_165028_tbl_chi_tiet_don_hang', 7);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_admin`
--

CREATE TABLE `tbl_admin` (
  `admin_id` int(10) UNSIGNED NOT NULL,
  `admin_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin_password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin_phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_admin`
--

INSERT INTO `tbl_admin` (`admin_id`, `admin_email`, `admin_password`, `admin_name`, `admin_phone`, `created_at`, `updated_at`) VALUES
(1, 'chuvandiep1999@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'Chu Van Diep', '0335659514', NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_bat_dong_san`
--

CREATE TABLE `tbl_bat_dong_san` (
  `id_bds` int(10) UNSIGNED NOT NULL,
  `id_du_an` int(11) DEFAULT NULL,
  `id_khu_vuc` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `ten_bds` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mo_ta_bds` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gia_bds` double DEFAULT NULL,
  `hinh_anh_bds` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hinh_anh_bds2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hinh_anh_bds3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `so_phong_ngu` int(11) DEFAULT NULL,
  `phap_ly` int(11) DEFAULT NULL,
  `so_nha_tam` int(11) DEFAULT NULL,
  `dien_tich` double(8,2) DEFAULT NULL,
  `tinh_trang_bds` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_bat_dong_san`
--

INSERT INTO `tbl_bat_dong_san` (`id_bds`, `id_du_an`, `id_khu_vuc`, `category_id`, `ten_bds`, `mo_ta_bds`, `gia_bds`, `hinh_anh_bds`, `hinh_anh_bds2`, `hinh_anh_bds3`, `so_phong_ngu`, `phap_ly`, `so_nha_tam`, `dien_tich`, `tinh_trang_bds`, `created_at`, `updated_at`) VALUES
(8, 3, 2, 3, 'Bán Nhà Cấp4, Hiệp Thành 13, 96M2 Giá rẻ Chỉ 6.6Tỷ, P.Hiệp Thành Q12.', '<p>Căn hộ gia đ&igrave;nh</p>\r\n\r\n<p>&quot;</p>', 150000000, 'nha487.jpg', 'nha590.jpg', 'nha637.jpg', 2, 1, 2, 80.00, 1, NULL, NULL),
(9, 5, 4, 5, 'Bán Đất Mặt Tiền 16m Gần Chợ-Gần Trung Tâm Hành Chính- Hổ Trợ Trả Góp', '<p>Cần b&aacute;n đất mặt tiền đường 16m với diện t&iacute;ch đất 5m x 16m =80m2 *Gi&aacute; b&aacute;n:1 tỷ 250 tr + Khu d&acirc;n cư đ&ocirc;ng đ&uacute;c, an ninh, m&ocirc;i trường sống y&ecirc;n tĩnh, trong l&agrave;nh. + Gần trường học cấp 1 - 2 - 3, UBND, trạm y tế, chợ... + Gần chợ B&igrave;nh Ch&aacute;nh, chợ đầu mối B&igrave;nh Điền, si&ecirc;u thị, trung t&acirc;m điện m&aacute;y, ng&acirc;n h&agrave;ng. - Hổ trợ trả g&oacute;p, Sổ hồng ri&ecirc;ng sang t&ecirc;n c&ocirc;ng chứng ngay Anh/chị e n&agrave;o quan t&acirc;m li&ecirc;n hệ em sđt : 0902.429.488 Ngọc Hậu để xem đất v&agrave; sổ&quot;&quot;</p>\r\n\r\n<p>&quot;</p>', 1250000000, 'phongtro1348.jpg', 'phongtro1267.jpg', 'phongtro107.jpg', NULL, 0, NULL, 80.00, 1, NULL, NULL),
(10, 3, 4, 3, 'BÁN NHÀ HẺM XE HƠI  Hẻm 1942 Huỳnh Tấn Phát Nhà Bè 5x13m', '<p>🅿️ B&aacute;n Nh&agrave; Hẻm Xe Hơi 1942 Huỳnh Tấn Ph&aacute;t Nh&agrave; B&egrave; __________💰 GI&Aacute; 4.28 TỶ TL ____________ ✔ Diện t&iacute;ch : 5mx13m ✔ Kết cấu : 1trệt lầu, 2 ph&ograve;ng ngủ, 2 vệ sinh, s&acirc;n để xe hơi, tặng sofa ph&ograve;ng kh&aacute;ch, b&agrave;n ăn. ✔ Hướng ch&iacute;nh đ&ocirc;ng 📚 Sổ hồng ho&agrave;n c&ocirc;ng đầy đủ hỗ trợ vay bank 💰 Gi&aacute; 4 Tỷ 280tr ( Thương Lượng ) ______________________________________ ☎️ Li&ecirc;n hệ or Zalo : 0901687978 Ph&uacute;c 🌐 Website : www.phucnhadat.vn * Xin vui l&ograve;ng li&ecirc;n hệ hoặc kết bạn zalo để được tư vấn tận t&igrave;nh v&agrave; chu đ&aacute;o. 🏠 PH&Uacute;C NH&Agrave; ĐẤT &mdash; TRAO GI&Aacute; TRỊ &mdash; NHẬN NIỀM TIN 🏠&quot;</p>\r\n\r\n<p>&quot;</p>', 4280000000, 'nha722.jpg', 'nha846.jpg', 'nha995.jpg', 2, 1, 2, 65.00, 1, NULL, NULL),
(11, 1, 4, 1, 'Bán Căn Hộ Scenic Valley , 77m2, 2pn view thoáng mất Giá rẻ', '<p>B&aacute;n căn hộ Scenic Valley , 2 ph&ograve;ng ngủ, 77m2 nh&agrave; đẹp, nội thất mới đẹp, đầy đủ nội thất. nh&agrave; đang trống xem nh&agrave; dễ, v&agrave; c&oacute; thể dọn v&agrave;o ở ngay. sổ hồng ph&aacute;p l&yacute; đầy đủ. gi&aacute; b&aacute;n chỉ 3.9 tỷ(gi&aacute; rẻ nhất thị trường) th&ocirc;ng tin chi tiết vui l&ograve;ng li&ecirc;n hệ : 0914 241 221 gặp Thư&quot;&quot;</p>\r\n\r\n<p>&quot;</p>', 3900000000, 'ggg_164843665419.jpg', 'hhh_164843665454.jpg', 'hhhhh_164843665488.jpg', 2, 1, 2, 77.00, 1, NULL, NULL),
(12, 1, 4, 1, 'Bán chung cư 78 Tân hoà đông - P14 - Q6 ( Lô A )', '<p>B&aacute;n chung cư 78 T&acirc;n ho&agrave; đ&ocirc;ng - P14 - Q6 ( L&ocirc; A ). Lầu 4 thang bộ bậc thang thấp rất dễ l&ecirc;n, 1trệt, lững, 3pn, 1pk, 1p bếp, 1wc. Gi&aacute; b&aacute;n : 1 tỷ 550 Xung quanh b&aacute;n k&iacute;nh 500m, căn hộ c&oacute; nhiều tiện &iacute;ch như: gần C&ocirc;ng vi&ecirc;n Ph&uacute; L&acirc;m, Galaxy Kinh Dương Vương, Khu Ẩm Thực v&agrave; nhiều cửa h&agrave;ng tiện lợi kh&aacute;c. Lh Hồng &Aacute;nh 0902780790, xem nh&agrave; b&aacute;o trước 60 ph&uacute;t.&quot;</p>\r\n\r\n<p>&quot;</p>', 1550000000, 'img-1649298930200-1649305534326_164930618081.jpg', 'img-1649298930234-1649305534615_164930622534.jpg', 'img-1649298930253-1649305534816_164930623843.jpg', 1, 0, 1, 50.00, 1, NULL, NULL),
(13, 1, 1, 1, 'Bán gấp - chủ đầu tư bán chung cư A5 phố Xã Đàn - Kim Liên - 560tr/căn', '<p>Kẹt tiền kinh doanh m&igrave;nh cần b&aacute;n căn hộ 77m2 Topaz Twins, căn 2PN, 2WC. Gi&aacute; 2,3 tỷ bao thuế ph&iacute; sang t&ecirc;n, gi&aacute; đ&atilde; bao gồm 5% đợi sổ + Căn hộ c&aacute;ch đường V&otilde; Thị S&aacute;u 200m. + C&aacute;ch BV ITO 180m. + Nằm tr&ecirc;n trục đường huyết mạch, tập trung nhiều khu an uống, qu&aacute;n cafe, nh&agrave; h&agrave;ng. Nh&agrave; trẻ, bệnh viện, trường học... Mọi chi tiết xin LH: 0933722992 (gặp Nguyệt) để được hỗ trợ v&agrave; tư vấn.&quot;&quot;</p>\r\n\r\n<p>&quot;</p>', 2400000000, 'canho412.jpg', 'canho552.jpg', 'canho652.jpg', 2, 1, 2, 77.00, 1, NULL, NULL),
(14, 1, 1, 1, 'Cần bán gấp căn hộ PROSPER PLAZA 65m2 2PN 2WC giá ngộp', '<p>Do dịch kẹt tiền n&ecirc;n b&aacute;n Prosper Plaza đ&atilde; c&oacute; sổ hồng. Ng&acirc;n h&agrave;ng hỗ trợ vay 70% tr&ecirc;n gi&aacute; trị sản phẩm. - Diện t&iacute;ch: 65m2 gồm 2 ph&ograve;ng ngủ v&agrave; 2 toilet, ban c&ocirc;ng. - Tầng thấp View hồ - Đ&atilde; c&oacute; sổ sang t&ecirc;n c&ocirc;ng chứng trong ng&agrave;y Gi&aacute; ngộp thấp hơn so với thị trường Chỉ 2.29 tỉ (C&ograve;n thượng lượng) A/c quan t&acirc;m li&ecirc;n hệ 0395.091.574(zalo) để được tư vấn v&agrave; hướng dẫn xem nh&agrave; trực tiếp.&quot;</p>', 2000000000, 'bds194.jpg', 'bds230.jpg', 'bds383.jpg', 3, 1, 3, 65.00, 1, NULL, NULL),
(15, 1, 2, 1, 'Cần bán căn hộ Topaz twins 78m2, ngay BV ITO Võ Thị Sáu, Biên Hòa', '<p>Kẹt tiền kinh doanh m&igrave;nh cần b&aacute;n căn hộ 77m2 Topaz Twins, căn 2PN, 2WC. Gi&aacute; 2,3 tỷ bao thuế ph&iacute; sang t&ecirc;n, gi&aacute; đ&atilde; bao gồm 5% đợi sổ. + Căn hộ c&aacute;ch đường V&otilde; Thị S&aacute;u 200m. + C&aacute;ch BV ITO 180m. + Nằm tr&ecirc;n trục đường huyết mạch, tập trung nhiều khu an uống, qu&aacute;n cafe, nh&agrave; h&agrave;ng. Nh&agrave; trẻ, bệnh viện, trường học... mọi chi tiết xin LH: 0933722992 (gặp Nguyệt) để được hỗ trợ v&agrave; tư vấn. Em c&oacute; nhận k&iacute; gửi cho thu&ecirc;, b&aacute;n căn hộ khu vực bi&ecirc;n h&ograve;a, cam kết gi&aacute; tốt nhất thị trường, anh chị cần b&aacute;n, cho thu&ecirc; LH:0933722992( gặp Nguyệt) B&aacute;o vi phạm&quot;&quot;</p>\r\n\r\n<p>&quot;</p>\r\n\r\n<p>&quot;</p>', 2229000000, 'canho42.jpg', 'canho134.jpg', 'canho378.jpg', 2, 0, 2, 78.00, 1, NULL, NULL),
(16, 3, 1, 3, '🅿️ BÁN NHÀ MẶT TIỀN 12M 🅿️ Đường Huỳnh Tấn Phát 4PN 5WC 4x18m', '<p>🅿️ B&aacute;n Nh&agrave; Mặt Tiền 12m Đường Huỳnh Tấn Ph&aacute;t Nh&agrave; B&egrave; C&aacute;ch Cầu Ph&uacute; Xu&acirc;n 300m &ndash;&ndash;&ndash;&ndash;&ndash;&ndash;&ndash; 💰 Gi&aacute; 10.5 TỶ TL &ndash;&ndash;&ndash;&ndash;&ndash;&ndash;&ndash; ✔ Vị Tr&iacute;: Đường trước nh&agrave; 20m, gần trường học si&ecirc;u thị, khu kinh doanh sầm uất , văn ph&ograve;ng h&agrave;nh ch&iacute;nh. ✔ Diện t&iacute;ch: 4x18m ✔ X&acirc;y dựng: 1 trệt 1 lửng 2 lầu, s&acirc;n để xe hơi, 1 ph&ograve;ng kh&aacute;ch, 1 ph&ograve;ng bếp, 4 ph&ograve;ng ngủ, 1 ph&ograve;ng thờ, ph&ograve;ng giặt, s&acirc;n thượng trước sau, nh&agrave; thiết kế lệch tầng hiện đại tho&aacute;ng m&aacute;t. ⚠️ Tặng lại tất cả nội thất như h&igrave;nh 📚 Ph&aacute;p l&yacute; sổ hồng ri&ecirc;ng. Ng&acirc;n h&agrave;ng hỗ trợ 70% 💰 Gi&aacute; : 10 tỷ 500tr Thương Lượng ______________________________________ ☎️ Li&ecirc;n hệ or Zalo : 0901687978 Ph&uacute;c 🌐 Website : www.phucnhadat.vn * Xin vui l&ograve;ng li&ecirc;n hệ hoặc kết bạn zalo để được tư vấn tận t&igrave;nh v&agrave; chu đ&aacute;o. 🏠 PH&Uacute;C NH&Agrave; ĐẤT &mdash; TRAO GI&Aacute; TRỊ &mdash; NHẬN NIỀM TIN 🏠&quot;</p>\r\n\r\n<p>&quot;</p>\r\n\r\n<p>&quot;</p>', 10000000000, 'nha1298.jpg', 'nha1098.jpg', 'nha1166.jpg', 4, 0, 4, 72.00, 1, NULL, NULL),
(17, 5, 1, 2, 'Bán lô đất đẹp khu đồng bộ Đường số 8, Hiệp bình Phước, Thủ Đức', '<p>B&aacute;n l&ocirc; đất đẹp khu ph&acirc;n l&ocirc; đồng bộ ( ngay phở Quỳnh Tr&acirc;m) Địa Chỉ: Đường số 8, Phường Hiệp B&igrave;nh Phước, Quận Thủ Đức Diện T&iacute;ch: 5 x 16.2 L&ocirc; đất hướng Đ&ocirc;ng Nam Ph&ugrave; hợp cho mua x&acirc;y dựng mới hoặc mua đầu tư kinh doanh Đường trước l&ocirc; đất 5m c&aacute;ch mặt tiền đường số 8 30m, c&aacute;ch mặt tiền đường Quốc Lộ 13 100m Gi&aacute; b&aacute;n: 6.5 Tỷ ( Thương lượng)&quot;</p>\r\n\r\n<p>&quot;</p>', 6000000000, 'matbang34.jpg', 'matbang13.jpg', 'matbang214.jpg', NULL, 1, NULL, 80.00, 1, NULL, NULL),
(18, 5, 2, 2, 'Cần bán nền đất kế góc 02 mặt tiền trong khu dân cư Bình Chánh TPHCM', '<p>Đất nền x&acirc;y kho xưởng hoặc nh&agrave; ở cho thu&ecirc; cao cấp. 2 l&ocirc; 5m x 21m = 105m2 ( 210m2 )nằm liền kề nhau, cho x&acirc;y tự do, sổ ri&ecirc;ng thổ cư ho&agrave;n to&agrave;n - Vị tr&iacute; kế qu&aacute;n Cafe NOW COFFEE nằm trong KDC T&ecirc;n Lửa mở rộng Tỉnh lộ 10 c&aacute;ch Bến xe miền t&acirc;y 15 p xe m&aacute;y - Xung quanh d&acirc;n cư đ&ocirc;ng đ&uacute;c, đầy đủ chợ - trường học - si&ecirc;u thị... - Sổ hồng ri&ecirc;ng sang t&ecirc;n ngay trong ng&agrave;y. Bao to&agrave;n bộ thuế ph&iacute; - Gi&aacute; b&aacute;n 1 tỷ 575 triệu / 105m2, mua 2 l&ocirc; thương lượng th&ecirc;m - Hỗ trợ ng&acirc;n h&agrave;ng 50-70%. Trả g&oacute;p 3 th&aacute;ng kh&ocirc;ng l&atilde;i suất. - Ngo&agrave;i ra c&ograve;n nền đất x&acirc;y dựng nh&agrave; phố kinh doanh đường nhựa xe oto tải ra v&agrave;o thoải m&aacute;i + Diện t&iacute;ch : 5m x 25m = 125m2 gi&aacute; 1 tỷ 900 triệu - Li&ecirc;n hệ : 0902.429.488 Gặp Ngọc Hậu ( Để đi xem thực tế đất v&agrave; sổ hồng )&quot;</p>\r\n\r\n<p>&quot;</p>\r\n\r\n<p>&quot;</p>\r\n\r\n<p>&quot;</p>', 1750000000, 'matbang371.jpg', 'matbang445.jpg', 'matbang538.jpg', NULL, 1, NULL, 105.00, 1, NULL, NULL),
(19, 1, 3, 1, 'Chung cư Cao Cấp Không muốn bán thì phải làm sao bây giờ', '<p>- Anh trai kẹt tiền cần b&aacute;n gấp căn hộ (25.5x78.5). Gi&aacute; chỉ với 739 triệu/1000m2. - Sổ hồng ri&ecirc;ng, c&ocirc;ng chứng ngay. - Đường hiện hữu, &ocirc; t&ocirc; v&ocirc; tận Nh&agrave; .&nbsp; Kh&ocirc;ng kh&iacute; trong l&agrave;nh, m&aacute;t mẻ. Th&iacute;ch hợp mua để đầu tư, nghĩ dưỡng... - C&aacute;ch quốc lộ, UBND, trường học, chợ, khu c&ocirc;ng nghiệp b&aacute;n k&iacute;nh 2,5km. - Li&ecirc;n hệ: 090. 365 1023_Vũ.&quot;</p>\r\n\r\n<p>&quot;</p>\r\n\r\n<p>&quot;</p>', 739000000, 'si199.jpg', 'si2-copy-04.jpg', 'si33.jpg', NULL, 1, NULL, 2000.00, 1, NULL, NULL),
(20, 5, 2, 5, 'San pham nay van laf Demoooooooooooooooooooooooo', '<p>dqaw</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&quot;</p>', 2000000000, '90507786-122325479379396-8493189173757345792-n_16492185144.jpg', '90297689-122325446046066-8715070852170776576-n_164921851438.jpg', '90790365-122325639379380-4093547739432878080-n_16492185146.jpg', 2, 0, 1, 11111.00, 1, NULL, NULL),
(22, 5, 4, 2, 'Đất Nền Quận 4 Demooooooooooooooooooo Can phai ban gap', '<p>Vị tr&iacute; đất ngay khu vực giao th&ocirc;ng thuận lợi, mặt tiền đường trục đường kết nối Nguyễn Hữu Thọ, Nguyễn Văn Linh, Nguyễn Lương Bằng, Nguyễn Thị Thập,<br />\r\n&nbsp;Diện t&iacute;ch: 1ha<br />\r\n&nbsp;Ph&ugrave; hợp ph&acirc;n l&ocirc; dự &aacute;n, đ&atilde; c&oacute; tổng thể mặt bằng ph&acirc;n l&ocirc;, mặt bằng giao th&ocirc;ng,...<br />\r\nPh&aacute;p l&yacute; đầy đủ, tư nh&acirc;n<br />\r\n&nbsp;Gi&aacute; tốt nhất thị trường<br />\r\n&nbsp;</p>\r\n\r\n<p>&quot;</p>\r\n\r\n<p>&quot;</p>', 298888800000, 'dat67.jpg', 'dat143.jpg', 'dat26.jpg', 4, 0, 3, 10000.00, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_chi_tiet_don_hang`
--

CREATE TABLE `tbl_chi_tiet_don_hang` (
  `id_chi_tiet_dh` int(10) UNSIGNED NOT NULL,
  `id_don_hang` int(11) NOT NULL,
  `id_bds` int(11) NOT NULL,
  `ten_bds` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gia_bds` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `so_luong_da_mua` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_chi_tiet_don_hang`
--

INSERT INTO `tbl_chi_tiet_don_hang` (`id_chi_tiet_dh`, `id_don_hang`, `id_bds`, `ten_bds`, `gia_bds`, `so_luong_da_mua`, `created_at`, `updated_at`) VALUES
(1, 3, 9, 'Bán Đất Mặt Tiền 16m Gần Chợ-Gần Trung Tâm Hành Chính- Hổ Trợ Trả Góp', '1250000000', 1, NULL, NULL),
(2, 3, 14, 'Cần bán gấp căn hộ PROSPER PLAZA 65m2 2PN 2WC giá ngộp', '2000000000', 5, NULL, NULL),
(3, 4, 20, 'Cuong', '2000000000', 1, NULL, NULL),
(4, 4, 9, 'Bán Đất Mặt Tiền 16m Gần Chợ-Gần Trung Tâm Hành Chính- Hổ Trợ Trả Góp', '1250000000', 1, NULL, NULL),
(5, 4, 15, 'Cần bán căn hộ Topaz twins 78m2, ngay BV ITO Võ Thị Sáu, Biên Hòa', '2229000000', 1, NULL, NULL),
(6, 4, 14, 'Cần bán gấp căn hộ PROSPER PLAZA 65m2 2PN 2WC giá ngộp', '2000000000', 1, NULL, NULL),
(7, 5, 20, 'Cuong', '2000000000', 1, NULL, NULL),
(8, 5, 9, 'Bán Đất Mặt Tiền 16m Gần Chợ-Gần Trung Tâm Hành Chính- Hổ Trợ Trả Góp', '1250000000', 1, NULL, NULL),
(9, 5, 15, 'Cần bán căn hộ Topaz twins 78m2, ngay BV ITO Võ Thị Sáu, Biên Hòa', '2229000000', 1, NULL, NULL),
(10, 5, 14, 'Cần bán gấp căn hộ PROSPER PLAZA 65m2 2PN 2WC giá ngộp', '2000000000', 1, NULL, NULL),
(11, 6, 15, 'Cần bán căn hộ Topaz twins 78m2, ngay BV ITO Võ Thị Sáu, Biên Hòa', '2229000000', 1, NULL, NULL),
(12, 6, 14, 'Cần bán gấp căn hộ PROSPER PLAZA 65m2 2PN 2WC giá ngộp', '2000000000', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_dat_coc`
--

CREATE TABLE `tbl_dat_coc` (
  `id_hinh_thuc` int(10) UNSIGNED NOT NULL,
  `hinh_thuc_dat_coc` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tinh_trang_dat_coc` varchar(50) CHARACTER SET utf8 NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_dat_coc`
--

INSERT INTO `tbl_dat_coc` (`id_hinh_thuc`, `hinh_thuc_dat_coc`, `tinh_trang_dat_coc`, `created_at`, `updated_at`) VALUES
(6, '0', 'Đang chờ xử lý', NULL, NULL),
(7, '0', 'Đang chờ xử lý', NULL, NULL),
(8, '0', 'Đang chờ xử lý', NULL, NULL),
(9, '0', 'Đang chờ xử lý', NULL, NULL),
(10, '0', 'Đang chờ xử lý', NULL, NULL),
(11, '0', 'Đang chờ xử lý', NULL, NULL),
(12, '0', 'Đang chờ xử lý', NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_don_hang`
--

CREATE TABLE `tbl_don_hang` (
  `id_don_hang` int(10) UNSIGNED NOT NULL,
  `id_khach_hang` int(11) NOT NULL,
  `id_dat_coc` int(11) NOT NULL,
  `id_hinh_thuc` int(11) NOT NULL,
  `tong_don_hang` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trang_thai_don_hang` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_don_hang`
--

INSERT INTO `tbl_don_hang` (`id_don_hang`, `id_khach_hang`, `id_dat_coc`, `id_hinh_thuc`, `tong_don_hang`, `trang_thai_don_hang`, `created_at`, `updated_at`) VALUES
(3, 5, 3, 6, '11,250,000,000.00', 'Đang chờ xử lý', NULL, NULL),
(4, 5, 4, 10, NULL, 'Đang chờ xử lý', NULL, NULL),
(5, 5, 5, 11, NULL, 'Đang chờ xử lý', NULL, NULL),
(6, 5, 6, 12, '4229000000', 'Đang chờ xử lý', NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_du_an`
--

CREATE TABLE `tbl_du_an` (
  `id_du_an` int(10) UNSIGNED NOT NULL,
  `ten_du_an` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mo_ta_du_an` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `tinh_trang_du_an` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_du_an`
--

INSERT INTO `tbl_du_an` (`id_du_an`, `ten_du_an`, `mo_ta_du_an`, `tinh_trang_du_an`, `created_at`, `updated_at`) VALUES
(1, 'Riviera Point', 'qqqqaaaaaaaa', 1, NULL, NULL),
(3, 'Saigon South Residences', 'Saigon South Residences là dự án căn hộ cao cấp đầu tiên của Chủ đầu tư Phú Mỹ Hưng triển khai tại Nhà Bè. Dự án sở hữu đầy đủ các tiêu chuẩn hàng đầu về chất lượng, tiện ích, cảnh quan cùng mức giá hợp lý trong phân khúc căn hộ cao cấp, nhận được đánh giá tốt và sự quan tâm của thị trường trong suốt 3 năm vừa qua.', 1, NULL, NULL),
(4, 'Sunrise Riverside', 'Sunrise Riverside là dự án căn hộ cao cấp đã hoàn thiện được tập đoàn Novaland thi công và phát triển. Đây được đánh giá là dự án nổi bật khu Nam Sài Gòn vào thời điểm năm 2017-2018, thu hút được nhiều sự quan tâm của thị trường và nhiều nhà đầu tư.', 1, NULL, NULL),
(5, 'Không thuộc dự án nào', 'Không có dự án nào đầu tư', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_khach_hang`
--

CREATE TABLE `tbl_khach_hang` (
  `id_khach_hang` int(10) UNSIGNED NOT NULL,
  `ten_khach_hang` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_khach_hang` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password_khach_hang` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sdt_khach_hang` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dia_chi_khach_hang` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_khach_hang`
--

INSERT INTO `tbl_khach_hang` (`id_khach_hang`, `ten_khach_hang`, `email_khach_hang`, `password_khach_hang`, `sdt_khach_hang`, `dia_chi_khach_hang`, `created_at`, `updated_at`) VALUES
(5, 'Phan Minh Cường', 'phanminhcuong@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', '1254777788', 'Lâm ĐỒng', NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_khu_vuc`
--

CREATE TABLE `tbl_khu_vuc` (
  `id_khu_vuc` int(10) UNSIGNED NOT NULL,
  `ten_khu_vuc` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tinh_trang_khu_vuc` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_khu_vuc`
--

INSERT INTO `tbl_khu_vuc` (`id_khu_vuc`, `ten_khu_vuc`, `tinh_trang_khu_vuc`, `created_at`, `updated_at`) VALUES
(1, 'Quận 1', 1, NULL, NULL),
(2, 'Quận 2', 1, NULL, NULL),
(3, 'Quận 3', 1, NULL, NULL),
(4, 'Quận 4', 1, NULL, NULL),
(6, 'Quận Bình Thạnh', 1, NULL, NULL),
(7, 'Quận Phú Nhuận', 1, NULL, NULL),
(8, 'Quận Tân Bình', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_theloai`
--

CREATE TABLE `tbl_theloai` (
  `category_id` int(10) UNSIGNED NOT NULL,
  `ten_loai` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mo_ta` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `tinh_trang` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_theloai`
--

INSERT INTO `tbl_theloai` (`category_id`, `ten_loai`, `mo_ta`, `tinh_trang`, `created_at`, `updated_at`) VALUES
(1, 'Căn hộ', 'Bán Căn hộ Việt Nam Giá Rẻ, Uy Tín, Chất Lượng', 1, NULL, NULL),
(2, 'Đất', 'Bán Căn hộ Việt Nam Giá Rẻ, Uy Tín, Chất Lượng', 1, NULL, NULL),
(3, 'Nhà', 'Bán Nhà Việt Nam Giá Rẻ, Uy Tín, Chất Lượng', 1, NULL, NULL),
(4, 'Mặt bằng', 'Bán mặt bằng Việt Nam Giá Rẻ, Uy Tín, Chất Lượng', 1, NULL, NULL),
(5, 'Phòng trọ', 'qqqq', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_thong_tin_dat_coc`
--

CREATE TABLE `tbl_thong_tin_dat_coc` (
  `id_dat_coc` int(10) UNSIGNED NOT NULL,
  `ten_dat_coc` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dia_chi_dat_coc` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sdt_dat_coc` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_dat_coc` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ghi_chu_dat_coc` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_thong_tin_dat_coc`
--

INSERT INTO `tbl_thong_tin_dat_coc` (`id_dat_coc`, `ten_dat_coc`, `dia_chi_dat_coc`, `sdt_dat_coc`, `email_dat_coc`, `ghi_chu_dat_coc`, `created_at`, `updated_at`) VALUES
(1, 'Chu Vawn Diep', 'Nghe An', '0555', 'sqsq@123.com', 'Cần gấp', NULL, NULL),
(2, 'Chu Van Diep', 'xom 3, nghe an', '0335659514', 'diepa2minbom@gmail.com', 'OKelun', NULL, NULL),
(3, 'Phan Minh Cường', 'Lâm Đồng', '012345678', 'cuong@gmail.com', NULL, NULL, NULL),
(4, 'Chu Văn Điệp', 'Nghệ An', '0335659514', 'chuvandiep1999@gmail.com', 'dwdwd', NULL, NULL),
(5, 'dwdwd', 'eqeqe', 'dqedqe', 'chuvandiep1999@gmail.com', 'dqqdq', NULL, NULL),
(6, 'Phan Minh Cường', 'Lâm Đồng', '0335659514', 'cuong@gmail.com', 'dwdwd', NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Chỉ mục cho bảng `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Chỉ mục cho bảng `tbl_bat_dong_san`
--
ALTER TABLE `tbl_bat_dong_san`
  ADD PRIMARY KEY (`id_bds`);

--
-- Chỉ mục cho bảng `tbl_chi_tiet_don_hang`
--
ALTER TABLE `tbl_chi_tiet_don_hang`
  ADD PRIMARY KEY (`id_chi_tiet_dh`);

--
-- Chỉ mục cho bảng `tbl_dat_coc`
--
ALTER TABLE `tbl_dat_coc`
  ADD PRIMARY KEY (`id_hinh_thuc`);

--
-- Chỉ mục cho bảng `tbl_don_hang`
--
ALTER TABLE `tbl_don_hang`
  ADD PRIMARY KEY (`id_don_hang`);

--
-- Chỉ mục cho bảng `tbl_du_an`
--
ALTER TABLE `tbl_du_an`
  ADD PRIMARY KEY (`id_du_an`);

--
-- Chỉ mục cho bảng `tbl_khach_hang`
--
ALTER TABLE `tbl_khach_hang`
  ADD PRIMARY KEY (`id_khach_hang`);

--
-- Chỉ mục cho bảng `tbl_khu_vuc`
--
ALTER TABLE `tbl_khu_vuc`
  ADD PRIMARY KEY (`id_khu_vuc`);

--
-- Chỉ mục cho bảng `tbl_theloai`
--
ALTER TABLE `tbl_theloai`
  ADD PRIMARY KEY (`category_id`);

--
-- Chỉ mục cho bảng `tbl_thong_tin_dat_coc`
--
ALTER TABLE `tbl_thong_tin_dat_coc`
  ADD PRIMARY KEY (`id_dat_coc`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT cho bảng `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `tbl_admin`
--
ALTER TABLE `tbl_admin`
  MODIFY `admin_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `tbl_bat_dong_san`
--
ALTER TABLE `tbl_bat_dong_san`
  MODIFY `id_bds` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT cho bảng `tbl_chi_tiet_don_hang`
--
ALTER TABLE `tbl_chi_tiet_don_hang`
  MODIFY `id_chi_tiet_dh` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT cho bảng `tbl_dat_coc`
--
ALTER TABLE `tbl_dat_coc`
  MODIFY `id_hinh_thuc` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT cho bảng `tbl_don_hang`
--
ALTER TABLE `tbl_don_hang`
  MODIFY `id_don_hang` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT cho bảng `tbl_du_an`
--
ALTER TABLE `tbl_du_an`
  MODIFY `id_du_an` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `tbl_khach_hang`
--
ALTER TABLE `tbl_khach_hang`
  MODIFY `id_khach_hang` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `tbl_khu_vuc`
--
ALTER TABLE `tbl_khu_vuc`
  MODIFY `id_khu_vuc` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT cho bảng `tbl_theloai`
--
ALTER TABLE `tbl_theloai`
  MODIFY `category_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT cho bảng `tbl_thong_tin_dat_coc`
--
ALTER TABLE `tbl_thong_tin_dat_coc`
  MODIFY `id_dat_coc` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
