<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblBatDongSan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_bat_dong_san', function (Blueprint $table) {
            $table->increments('id_bds');
            $table->integer('id_du_an');
            $table->integer('id_khu_vuc');
            $table->integer('category_id');
            $table->string('ten_bds');
            $table->text('mo_ta_bds');
            $table->double('gia_bds');
            $table->string('hinh_anh_bds');
            $table->integer('so_phong_ngu');
            $table->integer('phap_ly');
            $table->integer('so_nha_tam');
            $table->float('dien_tich');
            $table->integer('tinh_trang_bds');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_bat_dong_san');
    }
}
