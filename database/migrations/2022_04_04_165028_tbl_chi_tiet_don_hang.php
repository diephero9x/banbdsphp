<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TblChiTietDonHang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_chi_tiet_don_hang', function (Blueprint $table) {
            $table->increments('id_chi_tiet_dh');
            $table->integer('id_don_hang');
            $table->integer('id_bds');
            $table->string('ten_bds');
            $table->float('gia_bds');
            $table->integer('so_luong_da_mua');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_chi_tiet_don_hang');
    }
}
