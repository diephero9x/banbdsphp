<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblDuAn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_du_an', function (Blueprint $table) {
            $table->increments('id_du_an');
            $table->string('ten_du_an');
            $table->text('mo_ta_du_an');
            $table->integer('tinh_trang_du_an');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_du_an');
    }
}
