<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TblThongTinDatCoc extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_thong_tin_dat_coc', function (Blueprint $table) {
            $table->increments('id_dat_coc');
            $table->string('ten_dat_coc');
            $table->string('dia_chi_dat_coc');
            $table->string('sdt_dat_coc');
            $table->string('email_dat_coc');
            $table->integer('id_khach_hang');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_thong_tin_dat_coc');
    }
}
