<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TblDatCoc extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_dat_coc', function (Blueprint $table) {
            $table->increments('id_hinh_thuc');
            $table->string('hinh_thuc_dat_coc');
            $table->integer('tinh_trang_dat_coc');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_dat_coc');
    }
}
