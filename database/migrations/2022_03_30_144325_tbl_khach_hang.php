<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TblKhachHang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_khach_hang', function (Blueprint $table) {
            $table->increments('id_khach_hang');
            $table->string('ten_khach_hang');
            $table->string('email_khach_hang');
            $table->string('password_khach_hang');
            $table->string('sdt_khach_hang');
            $table->string('dia_chi_khach_hang');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_khach_hang');
    }
}
