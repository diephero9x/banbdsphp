<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TblDonHang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_don_hang', function (Blueprint $table) {
            $table->increments('id_don_hang');
            $table->integer('id_khach_hang');
            $table->integer('id_dat_coc');
            $table->integer('id_hinh_thuc');
            $table->float('tong_don_hang');
            $table->integer('trang_thai_don_hang');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_don_hang');
    }
}
