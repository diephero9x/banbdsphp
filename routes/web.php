<?php


use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//frontend
Route::get('/', 'HomeController@index');
Route::get('/trang-chu', 'HomeController@index');
Route::post('/tim-kiem', 'HomeController@tim_kiem');
//Loại bất động sản


//backend
Route::get('/admin', 'AdminController@index');
Route::get('/dashboard', 'AdminController@show_dashboard');
Route::get('/dangxuat', 'AdminController@dangxuat');
Route::post('/admin-dashboard', 'AdminController@dashboard');

//Danh muc nha dat
Route::get('/add-nha-dat', 'DanhMucSanPham@them');
Route::get('/all-nha-dat', 'DanhMucSanPham@lietke');
Route::get('/update-nha-dat/{category_id}', 'DanhMucSanPham@sua');
Route::get('/delete-nha-dat/{category_id}', 'DanhMucSanPham@xoa');

Route::get('/unactive-theloai/{category_id}', 'DanhMucSanPham@unactive_theloai');
Route::get('/active-theloai/{category_id}', 'DanhMucSanPham@active_theloai');

Route::post('/luu-the-loai', 'DanhMucSanPham@luu');
Route::post('/cap-nhat-the-loai/{category_id}', 'DanhMucSanPham@cap_nhat');

//Dự án
Route::get('/add-du-an', 'DuAnController@them');
Route::get('/all-du-an', 'DuAnController@lietke');
Route::get('/update-du-an/{id_du_an}', 'DuAnController@sua');
Route::get('/delete-du-an/{id_du_an}', 'DuAnController@xoa');

Route::get('/unactive-duan/{id_du_an}', 'DuAnController@unactive_duan');
Route::get('/active-duan/{id_du_an}', 'DuAnController@active_duan');

Route::post('/luu-du-an', 'DuAnController@luu');
Route::post('/cap-nhat-du-an/{id_du_an}', 'DuAnController@cap_nhat');

//Khu Vực
Route::get('/add-khu-vuc', 'KhuVucController@them');
Route::get('/all-khu-vuc', 'KhuVucController@lietke');
Route::get('/update-khu-vuc/{id_khu_vuc}', 'KhuVucController@sua');
Route::get('/delete-khu-vuc/{id_khu_vuc}', 'KhuVucController@xoa');

Route::get('/unactive-khuvuc/{id_khu_vuc}', 'KhuVucController@unactive_khuvuc');
Route::get('/active-khuvuc/{id_khu_vuc}', 'KhuVucController@active_khuvuc');

Route::post('/luu-khu-vuc', 'KhuVucController@luu');
Route::post('/cap-nhat-khu-vuc/{id_khu_vuc}', 'KhuVucController@cap_nhat');

//Bất Động Sản 
Route::get('/add-bds', 'SanPhamController@them');
Route::get('/all-bds', 'SanPhamController@lietke');
Route::get('/update-bds/{id_bds}', 'SanPhamController@sua');
Route::get('/delete-bds/{id_bds}', 'SanPhamController@xoa');

Route::get('/unactive-bds/{id_bds}', 'SanPhamController@unactive_bds');
Route::get('/active-bds/{id_bds}', 'SanPhamController@active_bds');
Route::post('/luu-bds', 'SanPhamController@luu');
Route::post('/cap-nhat-bds/{id_bds}', 'SanPhamController@cap_nhat');

Route::get('/chi-tiet-bds/{id_bds}', 'SanPhamController@chitietbds');

//Home
Route::get('/loai-bat-dong-san/{category_id}', 'DanhMucSanPham@danh_sach_bds_theo_loai');
Route::get('/du-an-bat-dong-san/{id_du_an}', 'DuAnController@danh_sach_bds_theo_du_an');
Route::get('/khu-vuc-bat-dong-san/{id_khu_vuc}', 'KhuVucController@danh_sach_bds_theo_khu_vuc');

//Giỏ hàng
Route::get('/hien-thi-gio-hang', 'GioHangController@hien_thi_ds_gio_hang');
Route::get('/hien-thi-gio-hang-ajax', 'GioHangController@hien_thi_ds_gio_hang_ajax');
Route::post('/luu-gio-hang', 'GioHangController@luu_gio_hang');
Route::get('/xoa-gio-hang/{rowId}', 'GioHangController@xoa_gio_hang');
Route::get('/xoa-gio-hang-all','GioHangController@xoa_gio_hang_all');

Route::get('/xoa-bds-gio-hang/{session_id}', 'GioHangController@xoa_bds_gio_hang');
Route::post('/cap-nhat-so-luong-cart', 'GioHangController@cap_nhat_so_luong_cart');
Route::post('/cap-nhat-gio-hang', 'GioHangController@cap_nhat_gio_hang');

Route::post('/add-cart-ajax', 'GioHangController@add_cart_ajax');


//Thanh toan
Route::get('/dang-nhap-thanh-toan', 'ThanhToanController@dang_nhap_thanh_toan');
Route::post('/dang-nhap-khach-hang', 'ThanhToanController@dang_nhap_khach_hang');
Route::post('/dang-ky-khach-hang', 'ThanhToanController@dang_ky_khach_hang');
Route::post('/tra-tien-dat-coc', 'ThanhToanController@tra_tien_dat_coc');
Route::get('/thanh-toan', 'ThanhToanController@thanh_toan');
Route::post('/luu-thanh-toan-dat-coc', 'ThanhToanController@luu_thanh_toan_dat_coc');
Route::get('/dang-xuat-khach-hang', 'ThanhToanController@dang_xuat_khach_hang');
Route::get('/dat-coc', 'ThanhToanController@dat_coc');

// Quản lý đơn hàng
Route::get('/quan-ly-don-hang', 'ThanhToanController@quan_ly_don_hang');
Route::get('/xem-don-hang/{id_don_hang}', 'ThanhToanController@xem_don_hang');
