@extends('layout')
@section('content')
    <div class="banner-bootom-w3-agileits py-5">
        <div class="container py-xl-4 py-lg-2">
            <!-- tittle heading -->
            <h3 class="tittle-w3l text-center mb-lg-5 mb-sm-4 mb-3">
                <span>C</span>hi
                <span>T</span>iết
            </h3>
            <div class="fb-share-button" data-href="http://localhost/banbdsphp/" data-layout="box_count" data-size="small"><a
                    target="_blank"
                    href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Flocalhost%2Fbanbdsphp%2F&amp;src=sdkpreparse"
                    class="fb-xfbml-parse-ignore">Chia sẻ</a></div>
            <div class="fb-like" data-href="http://localhost/banbdsphp/du-an-bat-dong-san/5" data-width=""
                data-layout="box_count" data-action="like" data-size="small" data-share="false"></div>
            <!-- //tittle heading -->
            <div class="row">

                @foreach ($chitietbds as $key => $chitietbds_id)
                    <div class="col-lg-5 col-md-8 single-right-left ">
                        <div class="grid images_3_of_2">
                            <div class="flexslider">
                                <ul class="slides">
                                    <li
                                        data-thumb="{{ URL::to('public/upload/batdongsan/' . $chitietbds_id->hinh_anh_bds) }}">
                                        <div class="thumb-image">
                                            <img src="{{ URL::to('public/upload/batdongsan/' . $chitietbds_id->hinh_anh_bds) }}"
                                                data-imagezoom="true" class="img-fluid" alt="">
                                        </div>
                                    </li>
                                    <li
                                        data-thumb="{{ URL::to('public/upload/batdongsan/' . $chitietbds_id->hinh_anh_bds2) }}">
                                        <div class="thumb-image">
                                            <img src="{{ URL::to('public/upload/batdongsan/' . $chitietbds_id->hinh_anh_bds2) }}"
                                                data-imagezoom="true" class="img-fluid" alt="">
                                        </div>
                                    </li>

                                    <li
                                        data-thumb="{{ URL::to('public/upload/batdongsan/' . $chitietbds_id->hinh_anh_bds3) }}">
                                        <div class="thumb-image">
                                            <img src="{{ URL::to('public/upload/batdongsan/' . $chitietbds_id->hinh_anh_bds3) }}"
                                                data-imagezoom="true" class="img-fluid" alt="">
                                        </div>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-7 single-right-left simpleCart_shelfItem">
                        <h3 class="mb-3">{{ $chitietbds_id->ten_bds }}</h3>
                        <p class="mb-3">
                            <span class="item_price">{{ number_format($chitietbds_id->gia_bds) }} VNĐ</span>

                        </p>
                        <div class="single-infoagile">
                            <ul>
                                <li class="mb-3">
                                    Mã ID bất động sản: {{ $chitietbds_id->id_bds }}
                                </li>
                                <li class="mb-3">
                                    Pháp lý: <span class="text-ellipsis">
                                        <?php
                                    if($chitietbds_id->phap_ly==0)
                                    {
                                        ?>
                                        Sổ đỏ
                                        <?php
                                            }else {
                                        ?>
                                        Sổ Hồng
                                        <?php
                                    }
                                ?>
                                    </span>
                                </li>
                                <li class="mb-3">
                                    Loại bất động sản: {{ $chitietbds_id->ten_loai }}
                                </li>
                                <li class="mb-3">
                                    Thuộc dự án: {{ $chitietbds_id->ten_du_an }}
                                </li>
                                <li class="mb-3">
                                    Thuộc khu vực: {{ $chitietbds_id->ten_khu_vuc }}
                                </li>
                            </ul>
                        </div>
                        <div class="product-single-w3l">
                            <p class="my-3">

                                <label>Thông Tin</label> Bất Động Sản
                            </p>
                            <ul>
                                <li class="mb-1">
                                    Diện tích: {{ $chitietbds_id->dien_tich }} m<sup>2</sup>
                                </li>
                                <li class="mb-1">
                                    Số phòng ngủ: {{ $chitietbds_id->so_phong_ngu }}
                                </li>
                                <li class="mb-1">
                                    Số nhà tắm: {{ $chitietbds_id->so_nha_tam }}
                                </li>
                            </ul>
                            <p class="my-sm-4 my-3">
                                <i class="fas fa-retweet mr-3"></i><b>Mô tả:</b> {{ $chitietbds_id->mo_ta_bds }}
                            </p>
                        </div>
                        <div class="occasion-cart">
                            <div class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out">

                                <form>
                                    @csrf
                                    <p style="color: black">Số lượng: </p>
                                    <input type="hidden" name="add" value="1" />
                                    <input type="number" value="1" min="1" style="width: 50px"
                                        class="form-control cart_product_qty_{{ $chitietbds_id->id_bds }}"/>
                                    <input type="hidden" value="{{ $chitietbds_id->id_bds }}"
                                        class="cart_product_id_{{ $chitietbds_id->id_bds }}">
                                    <input type="hidden" value="{{ $chitietbds_id->ten_bds }}"
                                        class="cart_product_name_{{ $chitietbds_id->id_bds }}">
                                    <input type="hidden" value="{{ $chitietbds_id->hinh_anh_bds }}"
                                        class="cart_product_image_{{ $chitietbds_id->id_bds }}">
                                    <input type="hidden" value="{{ $chitietbds_id->gia_bds }}"
                                        class="cart_product_price_{{ $chitietbds_id->id_bds }}">
                                    <input type="button" name="add-to-cart" data-id_bds="{{ $chitietbds_id->id_bds }}"
                                        value="Thêm giỏ hàng" class="button btn add-to-cart" />
                                </form>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
