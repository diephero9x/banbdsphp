@extends('layout')
@section('content')


    <div class="privacy py-sm-5 py-4">
        <div class="container py-xl-4 py-lg-2">
            <div class="card">
                <div class="row">
                    <div class="col-md-8 cart">
                        
                        <div class="title">
                            <div class="row">
                                <div class="col">
                                    <h4><b>Xem lại giỏ hàng</b></h4>
                                </div>
                                <div class="col align-self-center text-right text-muted"></div>
                            </div>
                        </div>
                        @php
                        $total1 = 0;
                    @endphp
                        @foreach (Session::get('cart') as $v_content)
                        @php
                                            $subtotal1 = $v_content['product_price'] * $v_content['product_qty'];
                                            $total1 += $subtotal1;
                                        @endphp
                        <div class="row border-top border-bottom">
                            <div class="row main align-items-center">
                                <div class="col-2"><img style="width: 3.5rem" class="img-fluid"
                                        src="{{ URL::to('public/upload/batdongsan/' . $v_content['product_image']) }}"></div>
                                <div class="col">
                                    <div class="row text-muted">{{ $v_content['product_name'] }}</div>
                                </div>
                                <div class="col">
                                    <p style="padding: 0 1vh; color: black" href="#" >{{ $v_content['product_qty']}}</p> </div>
                                <div class="col">  <?php
                                    $subtotal = $v_content['product_price'] * $v_content['product_qty'];
                                    echo number_format($subtotal) . ' ' . 'VNĐ';
                                    ?> <span class="close"></span></div>
                            </div>
                        </div>
                        @endforeach
                        <div class="back-to-shop"><a style="padding: 0 1vh;color: black" href="#">&leftarrow;</a><span
                                class="text-muted">Quay lại giỏ hàng</span>
                        </div>
                    </div>
                    <div class="col-md-4 summary">
                        <div>
                            <h5 style="margin-top: 4vh"><b>Tổng quan</b></h5>
                        </div>
                        <hr style="margin-top: 1.25rem">
                        <div class="row">
                            <div class="col" style="padding-left:0;">Tổng tiền:</div>
                            <div class="col text-right">{{$total1}}</div>
                        </div>
                        
                        <form style="padding: 2vh 0" method="POST" action="{{URL::to('/tra-tien-dat-coc')}}">
                            {{ csrf_field() }}
                            <p>Chọn loại đặt cọc</p> <select name="hinh_thuc_dat_coc" style=" border: 1px solid rgba(0, 0, 0, 0.137);
                        padding: 1.5vh 1vh;
                        margin-bottom: 4vh;
                        outline: none;
                        width: 100%;
                        background-color: rgb(247, 247, 247)">
                                <option class="text-muted" value="0">Thanh toán tiền mặt</option>
                                <option class="text-muted" value="1">Thanh toán bằng thẻ ATM</option>
                            </select>
                            <button class="btn">Đặt cọc</button>
                        </form>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>
        .card {
            margin: auto;
            max-width: 950px;
            width: 90%;
            box-shadow: 0 6px 20px 0 rgba(0, 0, 0, 0.19);
            border-radius: 1rem;
            border: transparent
        }

        @media(max-width:767px) {
            .card {
                margin: 3vh auto
            }
        }

        cart {
            background-color: #fff;
            padding: 4vh 5vh;
            border-bottom-left-radius: 1rem;
            border-top-left-radius: 1rem
        }

        @media(max-width:767px) {
            .cart {
                padding: 4vh;
                border-bottom-left-radius: unset;
                border-top-right-radius: 1rem
            }

        }

        .summary {
            background-color: #ddd;
            border-top-right-radius: 1rem;
            border-bottom-right-radius: 1rem;
            padding: 4vh;
            color: rgb(65, 65, 65)
        }

        @media(max-width:767px) {
            .summary {
                border-top-right-radius: unset;
                border-bottom-left-radius: 1rem
            }
        }

        .summary .col-2 {
            padding: 0
        }

        .summary .col-10 {
            padding: 0
        }

        .row {
            margin: 0
        }

        .title b {
            font-size: 1.5rem
        }

        .main {
            margin: 0;
            padding: 2vh 0;
            width: 100%
        }

        .col-2,
        .col {
            padding: 0 1vh
        }
        input:focus::-webkit-input-placeholder {
    color: transparent
}
.btn {
    background-color: #000;
    border-color: #000;
    color: white;
    width: 100%;
    font-size: 0.7rem;
    margin-top: 4vh;
    padding: 1vh;
    border-radius: 0
}
.btn:focus {
    box-shadow: none;
    outline: none;
    box-shadow: none;
    color: white;
    -webkit-box-shadow: none;
    -webkit-user-select: none;
    transition: none
}

.btn:hover {
    color: white
}
a:hover {
    color: black;
    text-decoration: none
}

#code {
    background-image: linear-gradient(to left, rgba(255, 255, 255, 0.253), rgba(255, 255, 255, 0.185)), url("https://img.icons8.com/small/16/000000/long-arrow-right.png");
    background-repeat: no-repeat;
    background-position-x: 95%;
    background-position-y: center
}
    </style>
@endsection
