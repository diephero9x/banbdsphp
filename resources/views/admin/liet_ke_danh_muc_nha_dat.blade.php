@extends('admin_layout')
@section('admin_content')
<div class="panel panel-widget">
    <div class="tables">
        <h4>Liệt kê danh mục:</h4>
        <?php
				
        $message = Session::get('message');
        if($message){
            echo '<span>',$message,'</span>';
            Session::put('message', null);
        }
    ?>
        <table class="table"> 
            <thead> 
                <tr> 
                    <th>#</th> 
                    <th>Tên danh mục nhà đất</th> 
                    <th>Hiển thị</th> 
                    
                    <th></th>
                </tr> 
            </thead> 
            <tbody> 
                    @foreach ( $danh_sach_the_loai as $key => $the_loai )
                        
                    
                    <tr class="active"> 
                        <th scope="row">{{$the_loai->category_id}}</th> 
                        <td>{{$the_loai->ten_loai}}</td> 
                        <td><span class="text-ellipsis">
                            <?php
                                if($the_loai->tinh_trang==0)
                                {
                                    ?>
                                    <a href="{{URL::to('/active-theloai/'.$the_loai->category_id)}}"><span class="fa-thumb-styling fa fa-thumbs-down"></span></a>
                                    </a>
                                    <?php
                                        }else {
                                    ?>
                                   
                                   <a href="{{URL::to('/unactive-theloai/'.$the_loai->category_id)}}">
                                    <span class="fa-thumb-styling fa fa-thumbs-up"></span>
                                   <?php
                                }
                            ?>
                            </span></td> 
                        
                        <td>
                            <a href="{{URL::to(('/update-nha-dat/'.$the_loai->category_id))}}" class="active styling-edit" ui-toggle-class="">
                                <i class="fa fa-pencil-square-o text-success text-active"></i>
                            </a>
                            
                            
                        </td>
                        <td>
                            <a onclick="return confirm('Bạn muốn xóa danh mục này không?')" href="{{URL::to(('/delete-nha-dat/'.$the_loai->category_id))}}" class="active styling-delete" ui-toggle-class="">
                                <i class="fa fa-times text-danger text"></i>
                            </a>
                        </td>
                    </tr> 
                   
                    @endforeach
                </tbody> 
        </table> 
    </div>
</div>
@endsection