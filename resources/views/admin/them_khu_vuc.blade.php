@extends('admin_layout')
@section('admin_content')
<div class="panel panel-widget forms-panel">
    <div class="forms">
        <div class="form-grids widget-shadow" data-example-id="basic-forms">
            <div class="form-title">
                <h4>Thêm khu vực :</h4>
            </div>
            <?php
					$message = Session::get('message');
					if($message){
						echo '<span>',$message,'</span>';
						Session::put('message', null);
					}
				?>
            <div class="form-body" >
                <form action="{{URL::to('luu-khu-vuc')}}" method="post">
                    {{ csrf_field() }}
                     <div class="form-group">
                          <label for="exampleInputEmail1">Tên khu vực</label>
                          <input type="text" name="ten_khu_vuc" class="form-control" id="exampleInputEmail1" placeholder="Tên khu vực">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Hiển thị</label>
                        <select name="tinh_trang_khu_vuc" id="selector1" class="form-control1">
                            <option value="0">Ẩn</option>
                            <option value="1">Hiển thị</option>
                        </select>
                    </div>
                    <button type="submit" name="them_khuvuc" class="btn btn-default">Thêm khu vực</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
