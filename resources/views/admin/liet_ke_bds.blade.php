@extends('admin_layout')
@section('admin_content')
<div class="panel panel-widget">
    <div class="tables">
        <h4>Liệt kê bất động sản:</h4>
        <?php
				
        $message = Session::get('message');
        if($message){
            echo '<span>',$message,'</span>';
            Session::put('message', null);
        }
    ?>
        <table class="table"> 
            <thead> 
                <tr> 
                    <th>#</th>
                    <th>Tên bất động sản</th> 
                    <th>Giá bất động sản</th> 
                    <th>Hình ảnh</th> 
                     
                    <th>Số phòng ngủ</th> 
                    <th>Pháp lý</th> 
                    <th>Số nhà tắm</th> 
                    <th>Diện tích</th> 
                    <th>Loại bất động sản</th> 
                    <th>Khu vực</th> 
                    <th>Dự án</th> 
                    <th>Hiển thị</th> 
                    
                    <th></th>
                </tr> 
            </thead> 
            <tbody> 
                    @foreach ( $danh_sach_bds as $key => $bds )
                    <tr class="active"> 
                        <th scope="row">{{$bds->id_bds}}</th> 
                        <td>{{$bds->ten_bds}}</td> 
                        <td>{{$bds->gia_bds}}</td> 
                        <td><img src="public/upload/batdongsan/{{$bds->hinh_anh_bds}}" height="200" width="200"></td> 
                         
                        <td>{{$bds->so_phong_ngu}}</td> 
                        <td><span class="text-ellipsis">
                            <?php
                                if($bds->phap_ly==0)
                                {
                                    ?>
                                    Sổ đỏ
                                    <?php
                                        }else {
                                    ?>
                                   Sổ Hồng
                                   <?php
                                }
                            ?>
                            </span></td> 

                        <td>{{$bds->so_nha_tam}}</td> 
                        <td>{{$bds->dien_tich}}</td> 
                        <td>{{$bds->ten_loai}}</td> 
                        <td>{{$bds->ten_khu_vuc}}</td> 
                        <td>{{$bds->ten_du_an}}</td> 
                        
                        <td><span class="text-ellipsis">
                            <?php
                                if($bds->tinh_trang_bds==0)
                                {
                                    ?>
                                    <a href="{{URL::to('/active-bds/'.$bds->id_bds)}}"><span class="fa-thumb-styling fa fa-thumbs-down"></span></a>
                                    </a>
                                    <?php
                                        }else {
                                    ?>
                                   
                                   <a href="{{URL::to('/unactive-bds/'.$bds->id_bds)}}">
                                    <span class="fa-thumb-styling fa fa-thumbs-up"></span>
                                   <?php
                                }
                            ?>
                            </span></td> 
                        <td>
                            <a href="{{URL::to(('/update-bds/'.$bds->id_bds))}}" class="active styling-edit" ui-toggle-class="">
                                <i class="fa fa-pencil-square-o text-success text-active"></i>
                            </a>
                            
                            
                        </td>
                        <td>
                            <a onclick="return confirm('Bạn muốn xóa bất động sản này không?')" href="{{URL::to(('/delete-bds/'.$bds->id_bds))}}" class="active styling-delete" ui-toggle-class="">
                                <i class="fa fa-times text-danger text"></i>
                            </a>
                        </td>
                    </tr> 
                   
                    @endforeach
                </tbody> 
        </table> 
    </div>
</div>
@endsection