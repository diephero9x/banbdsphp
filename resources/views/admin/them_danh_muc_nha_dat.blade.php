@extends('admin_layout')
@section('admin_content')
<div class="panel panel-widget forms-panel">
    <div class="forms">
        <div class="form-grids widget-shadow" data-example-id="basic-forms"> 
            <div class="form-title">
                <h4>Thêm danh mục nhà đất :</h4>
            </div>
            <?php
				
					$message = Session::get('message');
					if($message){
						echo '<span>',$message,'</span>';
						Session::put('message', null);
					}
				?>
            <div class="form-body" >
                <form action="{{URL::to('luu-the-loai')}}" method="post">
                    {{ csrf_field() }}
                     <div class="form-group">
                          <label for="exampleInputEmail1">Tên bất động sản</label> 
                          <input type="text" name="ten_danh_muc" class="form-control" id="exampleInputEmail1" placeholder="Tên bất động sản"> 
                    </div> 
                    <div class="form-group">
                        <label for="exampleInputPassword1">Mô tả</label> 
                        <textarea style="resize: none" rows="5" name="mo_ta" id="field-4" required="true" name="mo_ta_bat_dong_san" class="form-control at-required"></textarea>
                    </div>
                    <div class="form-group"> 
                        <label for="exampleInputEmail1">Hiển thị</label> 
                        <select name="tinh_trang" id="selector1" class="form-control1">
                            <option value="0">Ẩn</option>
                            <option value="1">Hiện thị</option>
                        </select>
                    </div> 
                    <button type="submit" name="them_bds" class="btn btn-default">Thêm danh mục</button>
                </form> 
            </div>
        </div>
    </div>
</div>
@endsection