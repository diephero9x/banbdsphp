@extends('admin_layout')
@section('admin_content')
<div class="panel panel-widget forms-panel">
    <div class="forms">
        <div class="form-grids widget-shadow" data-example-id="basic-forms"> 
            <div class="form-title">
                <h4>Thêm dự án :</h4>
            </div>
            <?php
				
					$message = Session::get('message');
					if($message){
						echo '<span>',$message,'</span>';
						Session::put('message', null);
					}
				?>
            <div class="form-body" >
                <form action="{{URL::to('luu-du-an')}}" method="post">
                    {{ csrf_field() }}
                     <div class="form-group">
                          <label for="exampleInputEmail1">Tên dự án</label> 
                          <input type="text" name="ten_du_an" class="form-control" id="exampleInputEmail1" placeholder="Tên dự án"> 
                    </div> 
                    <div class="form-group">
                        <label for="exampleInputPassword1">Mô tả</label> 
                        <textarea style="resize: none" rows="5" name="mo_ta" id="field-4" required="true" class="form-control at-required"></textarea>
                    </div>
                    <div class="form-group"> 
                        <label for="exampleInputEmail1">Hiển thị</label> 
                        <select name="tinh_trang_du_an" id="selector1" class="form-control1">
                            <option value="0">Ẩn</option>
                            <option value="1">Hiển thị</option>
                        </select>
                    </div> 
                    <button type="submit" name="them_duan" class="btn btn-default">Thêm dự án</button>
                </form> 
            </div>
        </div>
    </div>
</div>
@endsection