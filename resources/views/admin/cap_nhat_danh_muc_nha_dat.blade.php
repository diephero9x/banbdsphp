@extends('admin_layout')
@section('admin_content')
<div class="panel panel-widget forms-panel">
    <div class="forms">
        <div class="form-grids widget-shadow" data-example-id="basic-forms"> 
            <div class="form-title">
                <h4>Chỉnh sửa danh mục nhà đất :</h4>
            </div>
            <?php
					$message = Session::get('message');
					if($message){
						echo '<span>',$message,'</span>';
						Session::put('message', null);
					}
				?>
            <div class="form-body" >
                @foreach ($cap_nhat_the_loai as $key=> $cap_nhat_the_loai)
                    
                
                <form action="{{URL::to('cap-nhat-the-loai/'.$cap_nhat_the_loai->category_id)}}" method="post">
                    {{ csrf_field() }}
                     <div class="form-group">
                          <label for="exampleInputEmail1">Tên bất động sản</label> 
                          <input type="text" value="{{$cap_nhat_the_loai->ten_loai}}" name="ten_danh_muc" class="form-control" id="exampleInputEmail1" placeholder="Tên bất động sản"> 
                    </div> 
                    <div class="form-group">
                        <label for="exampleInputPassword1">Mô tả</label> 
                        <textarea style="resize: none"  rows="5" name="mo_ta" id="field-4" required="true" name="mo_ta_bat_dong_san" class="form-control at-required">{{$cap_nhat_the_loai->mo_ta}}</textarea>
                    </div>
                    
                    <button type="submit" name="cap-nhat-the-loai" class="btn btn-default">Cập nhật danh mục</button>
                </form> 
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection