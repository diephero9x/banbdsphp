@extends('admin_layout')
@section('admin_content')
<div class="panel panel-widget forms-panel">
    <div class="forms">
        <div class="form-grids widget-shadow" data-example-id="basic-forms"> 
            <div class="form-title">
                <h4>Chỉnh sửa khu vực :</h4>
            </div>
            <?php
					$message = Session::get('message');
					if($message){
						echo '<span>',$message,'</span>';
						Session::put('message', null);
					}
				?>
            <div class="form-body" >
                @foreach ($cap_nhat_khu_vuc as $key=> $cap_nhat_khu_vuc)
                    
                
                <form action="{{URL::to('cap-nhat-khu-vuc/'.$cap_nhat_khu_vuc->id_khu_vuc)}}" method="post">
                    {{ csrf_field() }}
                     <div class="form-group">
                          <label for="exampleInputEmail1">Tên khu vực</label> 
                          <input type="text" value="{{$cap_nhat_khu_vuc->ten_khu_vuc}}" name="ten_khu_vuc" class="form-control" id="exampleInputEmail1" placeholder="Tên khu vực"> 
                    </div> 
                    <button type="submit" name="cap-nhat-khu-vuc" class="btn btn-default">Cập nhật khu vực</button>
                </form> 
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection