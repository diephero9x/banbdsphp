@extends('admin_layout')
@section('admin_content')
<div class="panel panel-widget">
    <div class="tables">
        <h4>Liệt kê khu vực :</h4>
        <?php

        $message = Session::get('message');
        if($message){
            echo '<span>',$message,'</span>';
            Session::put('message', null);
        }
    ?>
        <table class="table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Tên khu vực</th>
                    <th>Hiển thị</th>

                    <th></th>
                </tr>
            </thead>
            <tbody>
                    @foreach ( $danh_sach_khu_vuc as $key => $khu_vuc )
                    <tr class="active">
                        <th scope="row">{{$khu_vuc->id_khu_vuc}}</th>
                        <td>{{$khu_vuc->ten_khu_vuc}}</td>
                        <td><span class="text-ellipsis">
                        
                            <?php
                                if($khu_vuc->tinh_trang_khu_vuc==0)
                                {
                                    ?>
                                    <a href="{{URL::to('/active-khuvuc/'.$khu_vuc->id_khu_vuc)}}"><span class="fa-thumb-styling fa fa-thumbs-down"></span></a>
                                    </a>
                                    <?php
                                        }else {
                                    ?>

                                   <a href="{{URL::to('/unactive-khuvuc/'.$khu_vuc->id_khu_vuc)}}">
                                    <span class="fa-thumb-styling fa fa-thumbs-up"></span>
                                   <?php
                                }
                            ?>
                            </span></td>
                        <td>
                            <a href="{{URL::to(('/update-khu-vuc/'.$khu_vuc->id_khu_vuc))}}" class="active styling-edit" ui-toggle-class="">
                                <i class="fa fa-pencil-square-o text-success text-active"></i>
                            </a>


                        </td>
                        <td>
                            <a onclick="return confirm('Bạn muốn xóa khu vực này không?')" href="{{URL::to(('/delete-khu-vuc/'.$khu_vuc->id_khu_vuc))}}" class="active styling-delete" ui-toggle-class="">
                                <i class="fa fa-times text-danger text"></i>
                            </a>
                        </td>
                    </tr>

                    @endforeach
                </tbody>
        </table>
    </div>
</div>
@endsection
