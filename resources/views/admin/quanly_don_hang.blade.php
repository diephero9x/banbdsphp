@extends('admin_layout')
@section('admin_content')
<div class="panel panel-widget">
    <div class="tables">
        <h4>Liệt kê đơn hàng:</h4>
        <?php
				
        $message = Session::get('message');
        if($message){
            echo '<span>',$message,'</span>';
            Session::put('message', null);
        }
    ?>
        <table class="table"> 
            <thead> 
                <tr> 
                    <th>#</th> 
                    <th>Tên người đặt</th> 
                    <th>Tổng số tiền</th> 
                    <th>Tình trạng</th> 
                    <th>Hiển Thị</th> 
                    <th></th>
                </tr> 
            </thead> 
            <tbody> 
                    @foreach ( $danh_sach_don_hang as $key => $don_hang )
                        
                    
                    <tr class="active"> 
                        <th scope="row">{{$don_hang->id_don_hang }}</th> 
                        <td>{{$don_hang->ten_khach_hang}}</td> 
                        <td>{{$don_hang->tong_don_hang}}</td> 
                        <td>{{$don_hang->trang_thai_don_hang}}</td> 
                        
                        <td>
                            <a href="{{URL::to(('/xem-don-hang/'.$don_hang->id_don_hang))}}" class="active styling-edit" ui-toggle-class="">
                                <i class="fa fa-pencil-square-o text-success text-active"></i>
                            </a>
                            
                            
                        </td>
                        <td>
                            <a onclick="return confirm('Bạn muốn xóa đơn hàng này không?')" href="{{URL::to(('/delete-don-hang/'.$don_hang->id_don_hang))}}" class="active styling-delete" ui-toggle-class="">
                                <i class="fa fa-times text-danger text"></i>
                            </a>
                        </td>
                    </tr> 
                   
                    @endforeach
                </tbody> 
        </table> 
    </div>
</div>
@endsection