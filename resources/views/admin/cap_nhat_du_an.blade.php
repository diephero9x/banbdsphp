@extends('admin_layout')
@section('admin_content')
<div class="panel panel-widget forms-panel">
    <div class="forms">
        <div class="form-grids widget-shadow" data-example-id="basic-forms"> 
            <div class="form-title">
                <h4>Chỉnh sửa dự án :</h4>
            </div>
            <?php
					$message = Session::get('message');
					if($message){
						echo '<span>',$message,'</span>';
						Session::put('message', null);
					}
				?>
            <div class="form-body" >
                @foreach ($cap_nhat_du_an as $key=> $cap_nhat_du_an)
                <form action="{{URL::to('cap-nhat-du-an/'.$cap_nhat_du_an->id_du_an)}}" method="post">
                    {{ csrf_field() }}
                     <div class="form-group">
                          <label for="exampleInputEmail1">Tên dự án</label> 
                          <input type="text" value="{{$cap_nhat_du_an->ten_du_an}}" name="ten_du_an" class="form-control" id="exampleInputEmail1" placeholder="Tên dự án"> 
                    </div> 
                    <div class="form-group">
                        <label for="exampleInputPassword1">Mô tả</label> 
                        <textarea style="resize: none"  rows="5" name="mo_ta_du_an" id="field-4" required="true" class="form-control at-required">{{$cap_nhat_du_an->mo_ta_du_an}}</textarea>
                    </div>
                    
                    <button type="submit" name="cap-nhat-du-an" class="btn btn-default">Cập nhật dự án</button>
                </form> 
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection