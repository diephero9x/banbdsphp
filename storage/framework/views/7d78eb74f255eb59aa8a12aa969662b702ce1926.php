
<?php $__env->startSection('admin_content'); ?>
<div class="panel panel-widget">
    <div class="tables">
        <h4>Liệt kê khu vực :</h4>
        <?php

        $message = Session::get('message');
        if($message){
            echo '<span>',$message,'</span>';
            Session::put('message', null);
        }
    ?>
        <table class="table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Tên khu vực</th>
                    <th>Hiển thị</th>

                    <th></th>
                </tr>
            </thead>
            <tbody>
                    <?php $__currentLoopData = $danh_sach_khu_vuc; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $khu_vuc): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr class="active">
                        <th scope="row"><?php echo e($khu_vuc->id_khu_vuc); ?></th>
                        <td><?php echo e($khu_vuc->ten_khu_vuc); ?></td>
                        <td><span class="text-ellipsis">
                        
                            <?php
                                if($khu_vuc->tinh_trang_khu_vuc==0)
                                {
                                    ?>
                                    <a href="<?php echo e(URL::to('/active-khuvuc/'.$khu_vuc->id_khu_vuc)); ?>"><span class="fa-thumb-styling fa fa-thumbs-down"></span></a>
                                    </a>
                                    <?php
                                        }else {
                                    ?>

                                   <a href="<?php echo e(URL::to('/unactive-khuvuc/'.$khu_vuc->id_khu_vuc)); ?>">
                                    <span class="fa-thumb-styling fa fa-thumbs-up"></span>
                                   <?php
                                }
                            ?>
                            </span></td>
                        <td>
                            <a href="<?php echo e(URL::to(('/update-khu-vuc/'.$khu_vuc->id_khu_vuc))); ?>" class="active styling-edit" ui-toggle-class="">
                                <i class="fa fa-pencil-square-o text-success text-active"></i>
                            </a>


                        </td>
                        <td>
                            <a onclick="return confirm('Bạn muốn xóa khu vực này không?')" href="<?php echo e(URL::to(('/delete-khu-vuc/'.$khu_vuc->id_khu_vuc))); ?>" class="active styling-delete" ui-toggle-class="">
                                <i class="fa fa-times text-danger text"></i>
                            </a>
                        </td>
                    </tr>

                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
        </table>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin_layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\banbdsphp\resources\views/admin/liet_ke_khu_vuc.blade.php ENDPATH**/ ?>