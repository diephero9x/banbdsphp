
<!DOCTYPE HTML>
<html>
<head>
<title>Đăng nhập admin</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Baxster Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- Bootstrap Core CSS -->
<link href="<?php echo e(asset('public/backend/css/bootstrap.css')); ?>" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="<?php echo e(asset('public/backend/css/style.css')); ?>" rel='stylesheet' type='text/css' />
<!-- font CSS -->
<link rel="icon" href="favicon.ico" type="image/x-icon" >
<!-- font-awesome icons -->
<link href="<?php echo e(asset('public/backend/css/font-awesome.css')); ?>" rel="stylesheet">
<!-- //font-awesome icons -->
<!--webfonts-->
<link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
<!--//webfonts-->
<!-- js -->
<script src="<?php echo e(asset('public/backend/js/jquery-1.11.1.min.js')); ?>"></script>
<!-- //js -->
</head>
<body class="login-bg">
		<div class="login-body">
			<div class="login-heading">
				<h1>Đăng nhập</h1>
				<?php
				//Kiểm tra đăng nhập đúng hay không
					$message = Session::get('message');
					if($message){
						echo '<span>',$message,'</span>';
						Session::put('message', null);
					}
				?>
			</div>
			<div class="login-info">
				<form action="<?php echo e(URL::to('admin-dashboard')); ?>" method="POST"><!-- gửi tới admin-dashboard -->
					<?php echo e(csrf_field()); ?><!-- dùng để bảo mật -->
					
					<input type="text" class="user" name="admin_email" placeholder="Tên đăng nhập" required="">
					<input type="password" name="admin_password" class="lock" placeholder="Mật khẩu">
					<div class="forgot-top-grids">
						<div class="forgot-grid">
							<ul>
								<li>
									<input type="checkbox" id="brand1" value="">
									<label for="brand1"><span></span>Ghi Nhớ</label>
								</li>
							</ul>
						</div>
						<div class="forgot">
							<a href="#">Quên mật khẩu?</a>
						</div>
						<div class="clearfix"> </div>
					</div>
					<input type="submit" name="Sign In" value="Đăng nhập">
					<div class="signup-text">
						<a href="signup.html">Không có tài khoản à? Tạo mới một cái nha.</a>
					</div>
					<hr>
					<h2>hoặc đăng nhập với</h2>
					<div class="login-icons">
						<ul>
							<li><a href="#" class="facebook"><i class="fa fa-facebook"></i></a></li>
							<li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
							<li><a href="#" class="google"><i class="fa fa-google-plus"></i></a></li>
							<li><a href="#" class="dribbble"><i class="fa fa-dribbble"></i></a></li>
						</ul>
					</div>
				</form>
			</div>
		</div>
		<div class="go-back login-go-back">
				<a href="index.html">Quay về trang chủ</a>
			</div>
		<div class="copyright login-copyright">
           <p>© 2016 Baxster . All Rights Reserved . Design by <a href="http://w3layouts.com/">W3layouts</a></p>
		</div>
</body>
</html>
<?php /**PATH C:\xampp\htdocs\banbdsphp\resources\views/admin_login.blade.php ENDPATH**/ ?>