
<?php $__env->startSection('admin_content'); ?>
<div class="panel panel-widget">
    <div class="tables">
        <h4>Liệt kê dự án:</h4>
        <?php
				
        $message = Session::get('message');
        if($message){
            echo '<span>',$message,'</span>';
            Session::put('message', null);
        }
    ?>
        <table class="table"> 
            <thead> 
                <tr> 
                    <th>#</th>
                    <th>Tên dự án</th> 
                    <th>Hiển thị</th> 
                    
                    <th></th>
                </tr> 
            </thead> 
            <tbody> 
                    <?php $__currentLoopData = $danh_sach_du_an; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $du_an): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr class="active"> 
                        <th scope="row"><?php echo e($du_an->id_du_an); ?></th> 
                        <td><?php echo e($du_an->ten_du_an); ?></td> 
                        <td><span class="text-ellipsis">
                            <?php
                                if($du_an->tinh_trang_du_an==0)
                                {
                                    ?>
                                    <a href="<?php echo e(URL::to('/active-duan/'.$du_an->id_du_an)); ?>"><span class="fa-thumb-styling fa fa-thumbs-down"></span></a>
                                    </a>
                                    <?php
                                        }else {
                                    ?>
                                   
                                   <a href="<?php echo e(URL::to('/unactive-duan/'.$du_an->id_du_an)); ?>">
                                    <span class="fa-thumb-styling fa fa-thumbs-up"></span>
                                   <?php
                                }
                            ?>
                            </span></td> 
                        <td>
                            <a href="<?php echo e(URL::to(('/update-du-an/'.$du_an->id_du_an))); ?>" class="active styling-edit" ui-toggle-class="">
                                <i class="fa fa-pencil-square-o text-success text-active"></i>
                            </a>
                            
                            
                        </td>
                        <td>
                            <a onclick="return confirm('Bạn muốn xóa dự án này không?')" href="<?php echo e(URL::to(('/delete-du-an/'.$du_an->id_du_an))); ?>" class="active styling-delete" ui-toggle-class="">
                                <i class="fa fa-times text-danger text"></i>
                            </a>
                        </td>
                    </tr> 
                   
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody> 
        </table> 
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin_layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\banbdsphp\resources\views/admin/liet_ke_du_an.blade.php ENDPATH**/ ?>