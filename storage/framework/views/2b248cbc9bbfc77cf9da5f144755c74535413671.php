
<?php $__env->startSection('admin_content'); ?>
<div class="panel panel-widget">
    <div class="tables">
        <h4>Liệt kê đơn hàng:</h4>
        <?php
				
        $message = Session::get('message');
        if($message){
            echo '<span>',$message,'</span>';
            Session::put('message', null);
        }
    ?>
        <table class="table"> 
            <thead> 
                <tr> 
                    <th>#</th> 
                    <th>Tên người đặt</th> 
                    <th>Tổng số tiền</th> 
                    <th>Tình trạng</th> 
                    <th>Hiển Thị</th> 
                    <th></th>
                </tr> 
            </thead> 
            <tbody> 
                    <?php $__currentLoopData = $danh_sach_don_hang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $don_hang): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        
                    
                    <tr class="active"> 
                        <th scope="row"><?php echo e($don_hang->id_don_hang); ?></th> 
                        <td><?php echo e($don_hang->ten_khach_hang); ?></td> 
                        <td><?php echo e($don_hang->tong_don_hang); ?></td> 
                        <td><?php echo e($don_hang->trang_thai_don_hang); ?></td> 
                        
                        <td>
                            <a href="<?php echo e(URL::to(('/xem-don-hang/'.$don_hang->id_don_hang))); ?>" class="active styling-edit" ui-toggle-class="">
                                <i class="fa fa-pencil-square-o text-success text-active"></i>
                            </a>
                            
                            
                        </td>
                        <td>
                            <a onclick="return confirm('Bạn muốn xóa đơn hàng này không?')" href="<?php echo e(URL::to(('/delete-don-hang/'.$don_hang->id_don_hang))); ?>" class="active styling-delete" ui-toggle-class="">
                                <i class="fa fa-times text-danger text"></i>
                            </a>
                        </td>
                    </tr> 
                   
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody> 
        </table> 
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin_layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\banbdsphp\resources\views/admin/quanly_don_hang.blade.php ENDPATH**/ ?>