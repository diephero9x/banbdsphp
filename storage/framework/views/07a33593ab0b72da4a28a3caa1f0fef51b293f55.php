
<?php $__env->startSection('admin_content'); ?>
    <div class="panel panel-widget forms-panel">
        <div class="forms">
            <div class="form-grids widget-shadow" data-example-id="basic-forms">
                <div class="form-title">
                    <h4>Cập Nhật Bất Động Sản :</h4>
                </div>
                <?php
                
                $message = Session::get('message');
                if ($message) {
                    echo '<span>', $message, '</span>';
                    Session::put('message', null);
                }
                ?>
                <div class="form-body">
                    <!-- multipart/form-data dung de gui ann -->
                    <?php $__currentLoopData = $cap_nhat_bds; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key =>$bds): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        
                   
                    <form action="<?php echo e(URL::to('/cap-nhat-bds/'.$bds->id_bds)); ?>" method="post" enctype="multipart/form-data">
                        <?php echo e(csrf_field()); ?>


                        <div class="form-group">
                            <label for="exampleInputEmail1">Tên Bất Động Sản</label>
                            <input type="text" name="ten_bds" class="form-control" id="exampleInputEmail1"
                                placeholder="Tên Bất Động Sản" value="<?php echo e($bds->ten_bds); ?>">
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1"> Loại Bất Động Sản  </label>
                            <select name="loai_bds" id="selector1" class="form-control1">
                                <?php $__currentLoopData = $ten_loai; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $loai): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php if($loai->category_id==$bds->category_id): ?>
                                    <option selected value="<?php echo e($loai->category_id); ?>"><?php echo e($loai->ten_loai); ?></option>
                                    <?php else: ?>
                                    <option  value="<?php echo e($loai->category_id); ?>"><?php echo e($loai->ten_loai); ?></option>
                                    <?php endif; ?>                           
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1"> Dự Án </label>
                            <select name="du_an_bds" id="selector1" class="form-control1">
                                <?php $__currentLoopData = $ten_du_an; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $duan): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php if($duan->id_du_an==$bds->category_id): ?>
                                    <option selected value="<?php echo e($duan->id_du_an); ?>"><?php echo e($duan->ten_du_an); ?></option>
                                   <?php else: ?>
                                   <option value="<?php echo e($duan->id_du_an); ?>"><?php echo e($duan->ten_du_an); ?></option>  
                                    <?php endif; ?>
                                    
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1"> Khu Vực </label>
                            <select name="khu_vuc_bds" id="selector1" class="form-control1">
                                <?php $__currentLoopData = $ten_khu_vuc; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $khuvuc): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php if($khuvuc->id_khu_vuc==$bds->id_khu_vuc): ?>
                                <option selected value="<?php echo e($khuvuc->id_khu_vuc); ?>"><?php echo e($khuvuc->ten_khu_vuc); ?></option>
                                <?php else: ?>
                                <option value="<?php echo e($khuvuc->id_khu_vuc); ?>"><?php echo e($khuvuc->ten_khu_vuc); ?></option>
                                <?php endif; ?>
                                
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                        </div>


                        <div class="form-group">
                            <label for="exampleInputEmail1">Hình Ảnh Bất Động Sản</label>
                            <input type="file" name="hinh_anh_bds" class="form-control" id="exampleInputEmail1"
                                placeholder="Hình Ảnh Bất Động Sảnn">
                                
                        </div>
                        <div class="form-group">
                            <img src="<?php echo e(URL::to('public/upload/batdongsan/'.$bds->hinh_anh_bds)); ?>" height="200" width="200">
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Hình Ảnh Bất Động Sản 2 </label>
                            <input type="file" name="hinh_anh_bds2" class="form-control" id="exampleInputEmail1"
                                placeholder="Hình Ảnh Bất Động Sảnn 2 ">
                                
                        </div>

                        <div class="form-group">
                            <img src="<?php echo e(URL::to('public/upload/batdongsan/'.$bds->hinh_anh_bds2)); ?>" height="200" width="200">
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Hình Ảnh Bất Động Sản 3 </label>
                            <input type="file" name="hinh_anh_bds3" class="form-control" id="exampleInputEmail1"
                                placeholder="Hình Ảnh Bất Động Sảnn 3">
                                
                        </div>

                        <div class="form-group">
                            <img src="<?php echo e(URL::to('public/upload/batdongsan/'.$bds->hinh_anh_bds3)); ?>" height="200" width="200">
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Giá Thành</label>
                            <input type="text" name="gia_bds" class="form-control" id="exampleInputEmail1"
                                placeholder="Giá Thành" value="<?php echo e($bds->gia_bds); ?>">
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Số Phòng Ngủ </label>
                            <input type="number" name="so_phong_ngu" class="form-control" id="exampleInputEmail1"
                                placeholder="Số Phòng Ngủ" value="<?php echo e($bds->so_phong_ngu); ?>">
                        </div>

                        
                        <div class="form-group">
                            <label for="exampleInputEmail1">Pháp Lý </label>
                            <select name="phap_ly" id="selector1" class="form-control1">
                                <?php
                                if($bds->phap_ly==0)
                                {
                                    ?>
                                    <option value="0">Sổ Đỏ</option>
                                    <?php
                                        }else {
                                    ?>
                                   <option value="1">Sổ Hồng</option>
                                   <?php
                                }
                                
                            ?>
                            <option value="0">Sổ Đỏ</option>
                            <option value="1">Sổ Hồng</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Số Nhà Tắm</label>
                            <input type="number" name="so_nha_tam" class="form-control" id="exampleInputEmail1"
                                placeholder="Số Nhà Tắm" value="<?php echo e($bds->so_nha_tam); ?>">
                        </div>


                        <div class="form-group">
                            <label for="exampleInputEmail1">Diện Tích</label>
                            <input type="text" name="dien_tich" class="form-control" id="exampleInputEmail1"
                                placeholder="Diện Tích" value="<?php echo e($bds->dien_tich); ?>">
                        </div> 


                        <div class="form-group">
                            <label for="exampleInputPassword1"> Mô Tả </label>
                            <textarea style="resize: none" rows="5" name="mo_ta" id="field-4" required="true"
                            class="ckeditor" ><?php echo e($bds->mo_ta_bds); ?>"</textarea>
                        </div>
                        <button type="submit" name="them_bds" class="btn btn-default">Cập Nhật Bất Động Sản</button>
                    </form>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
f>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin_layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\banbdsphp\resources\views/admin/cap_nhat_bds.blade.php ENDPATH**/ ?>