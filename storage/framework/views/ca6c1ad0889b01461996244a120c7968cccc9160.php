
<?php $__env->startSection('content'); ?>
    <div class="ads-grid py-sm-5 py-4">
        <div class="container py-xl-4 py-lg-2">
            <!-- tittle heading -->
            <h3 class="tittle-w3l text-center mb-lg-5 mb-sm-4 mb-3">
                <span>B</span>ất
                <span>Đ</span>ộng
                <span>S</span>ản
            </h3>
            <!-- //tittle heading -->
            <div class="row">

                <!-- product left -->
                <div class="agileinfo-ads-display col-lg-9">
                    <div class="wrapper">
                        <!-- first section -->
                        <div class="product-sec1 px-sm-4 px-3 py-sm-5  py-3 mb-4">
                            <h3 class="heading-tittle text-center font-italic">Căn hộ mới nhất</h3>
                            <div class="row">
                                <?php $__currentLoopData = $canho; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $canho): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="col-md-4 product-men mt-5">
                                        <div class="men-pro-item simpleCart_shelfItem">
                                            <div class="men-thumb-item text-center">

                                                <img src="<?php echo e(URL::to('public/upload/batdongsan/' . $canho->hinh_anh_bds)); ?>"
                                                    width="150px" height="200px" alt="">


                                                <div class="men-cart-pro">
                                                    <div class="inner-men-cart-pro">
                                                        <a href="<?php echo e(URL::to('/chi-tiet-bds/' . $canho->id_bds)); ?>"
                                                            class="link-product-add-cart">Chi tiết</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item-info-product text-center border-top mt-4">
                                                <h4 class="pt-1">
                                                    <a
                                                        href="<?php echo e(URL::to('/chi-tiet-bds/' . $canho->id_bds)); ?>"><?php echo e($canho->ten_bds); ?></a>
                                                </h4>
                                                <div class="info-product-price my-2">
                                                    <span
                                                        class="item_price"><?php echo e(number_format($canho->gia_bds) . ' ' . 'VNĐ'); ?></span>

                                                </div>
                                                <div
                                                    class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out">
                                                    <form>
                                                        <?php echo csrf_field(); ?>
                                                        <input type="hidden" value="<?php echo e($canho->id_bds); ?>"
                                                            class="cart_product_id_<?php echo e($canho->id_bds); ?>">
                                                        <input type="hidden" value="<?php echo e($canho->ten_bds); ?>"
                                                            class="cart_product_name_<?php echo e($canho->id_bds); ?>">
                                                        <input type="hidden" value="<?php echo e($canho->hinh_anh_bds); ?>"
                                                            class="cart_product_image_<?php echo e($canho->id_bds); ?>">
                                                        <input type="hidden" value="<?php echo e($canho->gia_bds); ?>"
                                                            class="cart_product_price_<?php echo e($canho->id_bds); ?>">
                                                        <input type="hidden" value="1"
                                                            class="cart_product_qty_<?php echo e($canho->id_bds); ?>">
                                                        <input type="button" name="add-to-cart"
                                                            data-id_bds="<?php echo e($canho->id_bds); ?>" value="Thêm giỏ hàng"
                                                            class="button btn add-to-cart" />
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                            </div>
                        </div>
                        <!-- //first section -->
                        <!-- second section -->
                        <div class="product-sec1 px-sm-4 px-3 py-sm-5  py-3 mb-4">
                            <h3 class="heading-tittle text-center font-italic">Nhà mới nhất</h3>
                            <div class="row">
                                <?php $__currentLoopData = $nha; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $nha): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="col-md-4 product-men mt-5">
                                        <div class="men-pro-item simpleCart_shelfItem">
                                            <div class="men-thumb-item text-center">
                                                <img src="<?php echo e(URL::to('public/upload/batdongsan/' . $nha->hinh_anh_bds)); ?>"
                                                    width="150px" height="200px" alt="">
                                                <div class="men-cart-pro">
                                                    <div class="inner-men-cart-pro">
                                                        <a href="<?php echo e(URL::to('/chi-tiet-bds/' . $nha->id_bds)); ?>"
                                                            class="link-product-add-cart">Chi tiết</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item-info-product text-center border-top mt-4">
                                                <h4 class="pt-1">
                                                    <a
                                                        href="<?php echo e(URL::to('/chi-tiet-bds/' . $nha->id_bds)); ?>"><?php echo e($nha->ten_bds); ?></a>
                                                </h4>
                                                <div class="info-product-price my-2">
                                                    <span class="item_price"><span
                                                            class="item_price"><?php echo e(number_format($nha->gia_bds) . ' ' . 'VNĐ'); ?></span></span>

                                                </div>
                                                <div
                                                    class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out">
                                                    <form>
                                                        <?php echo csrf_field(); ?>
                                                        <input type="hidden" value="<?php echo e($canho->id_bds); ?>"
                                                            class="cart_product_id_<?php echo e($canho->id_bds); ?>">
                                                        <input type="hidden" value="<?php echo e($canho->ten_bds); ?>"
                                                            class="cart_product_name_<?php echo e($canho->id_bds); ?>">
                                                        <input type="hidden" value="<?php echo e($canho->hinh_anh_bds); ?>"
                                                            class="cart_product_image_<?php echo e($canho->id_bds); ?>">
                                                        <input type="hidden" value="<?php echo e($canho->gia_bds); ?>"
                                                            class="cart_product_price_<?php echo e($canho->id_bds); ?>">
                                                        <input type="hidden" value="1"
                                                            class="cart_product_qty_<?php echo e($canho->id_bds); ?>">
                                                        <input type="button" name="add-to-cart"
                                                            data-id_bds="<?php echo e($canho->id_bds); ?>" value="Thêm giỏ hàng"
                                                            class="button btn add-to-cart" />
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>
                        </div>
                        <!-- //second section -->
                        <!-- third section -->
                        <div class="product-sec1 product-sec2 px-sm-5 px-3">
                            <div class="row">
                                <h3 class="col-md-4 effect-bg"> Diversity </h3>
                                
                                <div class="col-md-8 bg-right-nut">
                                    <img style=width: "7000px" , height="344px"
                                        src="<?php echo e(asset('public/frontend/images/image1.png')); ?>" alt="">
                                </div>
                            </div>
                        </div>
                        <!-- //third section -->
                        <!-- fourth section -->
                        <div class="product-sec1 px-sm-4 px-3 py-sm-5  py-3 mt-4">
                            <h3 class="heading-tittle text-center font-italic">Đất nền mới nhất</h3>
                            <div class="row">
                                <?php $__currentLoopData = $dat; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $dat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="col-md-4 product-men mt-5">
                                        <div class="men-pro-item simpleCart_shelfItem">
                                            <div class="men-thumb-item text-center">
                                                <img src="<?php echo e(URL::to('public/upload/batdongsan/' . $dat->hinh_anh_bds)); ?>"
                                                    width="150px" height="200px" alt="">
                                                <div class="men-cart-pro">
                                                    <div class="inner-men-cart-pro">
                                                        <a href="<?php echo e(URL::to('/chi-tiet-bds/' . $dat->id_bds)); ?>"
                                                            class="link-product-add-cart">Quick View</a>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="item-info-product text-center border-top mt-4">
                                                <h4 class="pt-1">
                                                    <a
                                                        href="<?php echo e(URL::to('/chi-tiet-bds/' . $dat->id_bds)); ?>"><?php echo e($dat->ten_bds); ?></a>
                                                </h4>
                                                <div class="info-product-price my-2">
                                                    <span class="item_price"><span
                                                            class="item_price"><?php echo e(number_format($dat->gia_bds) . ' ' . 'VNĐ'); ?></span></span>

                                                </div>
                                                <div
                                                    class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out">
                                                    <form>
                                                        <?php echo csrf_field(); ?>
                                                        <input type="hidden" value="<?php echo e($canho->id_bds); ?>"
                                                            class="cart_product_id_<?php echo e($canho->id_bds); ?>">
                                                        <input type="hidden" value="<?php echo e($canho->ten_bds); ?>"
                                                            class="cart_product_name_<?php echo e($canho->id_bds); ?>">
                                                        <input type="hidden" value="<?php echo e($canho->hinh_anh_bds); ?>"
                                                            class="cart_product_image_<?php echo e($canho->id_bds); ?>">
                                                        <input type="hidden" value="<?php echo e($canho->gia_bds); ?>"
                                                            class="cart_product_price_<?php echo e($canho->id_bds); ?>">
                                                        <input type="hidden" value="1"
                                                            class="cart_product_qty_<?php echo e($canho->id_bds); ?>">
                                                        <input type="button" name="add-to-cart"
                                                            data-id_bds="<?php echo e($canho->id_bds); ?>" value="Thêm giỏ hàng"
                                                            class="button btn add-to-cart" />
                                                    </form>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


                            </div>
                        </div>
                        <!-- //fourth section -->


                    </div>
                </div>
                <!-- //product left -->

                <!-- product right -->
                <div class="col-lg-3 mt-lg-0 mt-4 p-lg-0">
                    <div class="side-bar p-sm-4 p-3">
                        <div class="search-hotel border-bottom py-2">
                            <h3 class="agileits-sear-head mb-3">Search Here..</h3>
                            <form action="#" method="post">
                                <input type="search" placeholder="Product name..." name="search" required="">
                                <input type="submit" value=" ">
                            </form>
                        </div>
                        <!-- price -->
                        <div class="range border-bottom py-2">
                            <h3 class="agileits-sear-head mb-3"> Tầm Giá </h3>
                            <div class="w3l-range">
                                <ul>
                                    <li>
                                        <a href="#">Dưới 100 triệu </a>
                                    </li>
                                    <li class="my-1">
                                        <a href="#">100 triệu - 999 triệu </a>
                                    </li>
                                    <li>
                                        <a href="#">trên 1 tỷ</a>
                                        
                                </ul>
                            </div>
                        </div>
                        <!-- //price -->
                        <!-- discounts -->
                        <div class="left-side border-bottom py-2">
                            <h3 class="agileits-sear-head mb-3">Giảm Giá </h3>
                            <ul>
                                <li>
                                    <input type="checkbox" class="checked">
                                    <span class="span">5% </span>
                                </li>
                                <li>
                                    <input type="checkbox" class="checked">
                                    <span class="span">10% </span>
                                </li>
                                <li>
                                    <input type="checkbox" class="checked">
                                    <span class="span">20% </span>
                                </li>
                                
                            </ul>
                        </div>
                        <!-- //discounts -->
                        <!-- reviews -->
                        <div class="customer-rev border-bottom left-side py-2">
                            <h3 class="agileits-sear-head mb-3"> Đánh Giá </h3>
                            <ul>
                                <li>
                                    <a href="#">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <span>5.0</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <span>4.0</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star-half"></i>
                                        <span>3.5</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <span>3.0</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star-half"></i>
                                        <span>2.5</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <!-- //reviews -->
                        <!-- electronics -->
                        <div class="left-side border-bottom py-2">
                            <h3 class="agileits-sear-head mb-3">TIện Ích </h3>
                            <ul>
                                <li>
                                    <input type="checkbox" class="checked">
                                    <span class="span"> Mặt Tiền </span>
                                </li>
                                <li>
                                    <input type="checkbox" class="checked">
                                    <span class="span"> Có Ban CÔng </span>
                                </li>
                                <li>
                                    <input type="checkbox" class="checked">
                                    <span class="span"> rung Tâm Thành Phố </span>
                                </li>
                                <li>
                                    <input type="checkbox" class="checked">
                                    <span class="span"> Phòng Tắm Riêng </span>
                                </li>
                                <li>
                                    <input type="checkbox" class="checked">
                                    <span class="span"> Hầm Gửi Xe </span>
                                </li>
                                <li>
                                    <input type="checkbox" class="checked">
                                    <span class="span"> Có Gác </span>
                                </li>
                                <li>
                                    <input type="checkbox" class="checked">
                                    <span class="span">Có Bảo Vệ </span>
                                </li>
                                
                            </ul>
                        </div>
                        <!-- //electronics -->
                        <!-- delivery -->
                        <div class="left-side border-bottom py-2">
                            <h3 class="agileits-sear-head mb-3">Payment</h3>
                            <ul>
                                <li>
                                    <input type="checkbox" class="checked">
                                    <span class="span"> Thanh Toán Nhận Nhà </span>
                                </li>
                            </ul>

                            <ul>
                                <li>
                                    <input type="checkbox" class="checked">
                                    <span class="span">Hợp Đồng Trả Góp</span>
                                </li>
                            </ul>

                        </div>
                        <!-- //delivery -->
                        <!-- arrivals -->
                        <div class="left-side border-bottom py-2">
                            <h3 class="agileits-sear-head mb-3">Khu Vực HOT</h3>
                            <ul>
                                <li>
                                    <input type="checkbox" class="checked">
                                    <span class="span">Quận 2</span>
                                </li>
                                <li>
                                    <input type="checkbox" class="checked">
                                    <span class="span">Quận 3</span>
                                </li>
                            </ul>
                        </div>
                        <!-- //arrivals -->
                        <!-- best seller -->
                        <div class="f-grid py-2">
                            <h3 class="agileits-sear-head mb-3">Loại Hình Nổi Bật </h3>
                            <div class="box-scroll">
                                <div class="scroll">
                                    <div class="row">
                                        <div class="col-lg-3 col-sm-2 col-3 left-mar">
                                            <img src="<?php echo e(asset('public/frontend/images/k1.jpg')); ?>" alt=""
                                                class="img-fluid">
                                        </div>
                                        <div class="col-lg-9 col-sm-10 col-9 w3_mvd">
                                            <a href="">Chung Cư Sang Trọng </a>
                                            <a href="" class="price-mar mt-2">1.100,990.000</a>
                                        </div>
                                    </div>
                                    <div class="row my-4">
                                        <div class="col-lg-3 col-sm-2 col-3 left-mar">
                                            <img src="<?php echo e(asset('public/frontend/images/k2.jpg')); ?>" alt=""
                                                class="img-fluid">
                                        </div>
                                        <div class="col-lg-9 col-sm-10 col-9 w3_mvd">
                                            <a href="">Phòng Trọ Sinh Viên </a>
                                            <a href="" class="price-mar mt-2">5,499.000</a>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-3 col-sm-2 col-3 left-mar">
                                            <img src="<?php echo e(asset('public/frontend/images/k3.jpg')); ?>" alt=""
                                                class="img-fluid">
                                        </div>
                                        <div class="col-lg-9 col-sm-10 col-9 w3_mvd">
                                            <a href="">Nhà Nguyên Căn Tiện Lợi </a>
                                            <a href="" class="price-mar mt-2">2.200.199.000 </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- //best seller -->
                    </div>
                    <!-- //product right -->
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\banbdsphp\resources\views/pages/home.blade.php ENDPATH**/ ?>