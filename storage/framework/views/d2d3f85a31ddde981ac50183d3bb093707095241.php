
<?php $__env->startSection('content'); ?>
    <div class="privacy py-sm-5 py-4">
        <div class="container py-xl-4 py-lg-2">
            <!-- tittle heading -->
            <h3 class="tittle-w3l text-center mb-lg-5 mb-sm-4 mb-3">
                <span>G</span>iỏ hàng
            </h3>
            <?php
            $content = Cart::content();
            ?>
            <!-- //tittle heading -->
            <div class="checkout-right">
                <h4 class="mb-sm-4 mb-3">Your shopping cart contains:
                    <span>3 Products</span>
                </h4>
                <div class="table-responsive">
                    <table class="timetable_sub">
                        <thead>
                            <tr>
                                <th>Mã BĐS</th>
                                <th>Hình ảnh</th>
                                <th>Số lượng</th>
                                <th>Tên</th>
                                <th>Tổng Giá</th>
                                <th>Remove</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $__currentLoopData = $content; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $v_content): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr class="rem1">
                                    <td class="invert"><?php echo e($v_content->id); ?></td>
                                    <td class="invert-image">
                                        <a href="single.html">
                                            <img src="<?php echo e(URL::to('public/upload/batdongsan/' . $v_content->options->image)); ?>"
                                                alt=" " width="86px" height="86px" class="img-responsive">
                                        </a>
                                    </td>
                                    <td class="invert">
                                        <div class="input-group mb-3">
                                            <form class="input-group mb-3"
                                                action="<?php echo e(URL::to('/cap-nhat-so-luong-cart')); ?>" method="POST">
                                                <?php echo e(csrf_field()); ?>

                                                <input type="number" name="cart_quantity" style="width: 50px; height: 40px"
                                                    class="form-control" value="<?php echo e($v_content->qty); ?>">
                                                <input type="hidden" name="rowID_cart" class="form-control"
                                                    value="<?php echo e($v_content->rowId); ?>">
                                                <input class="btn btn-outline-secondary" style="background: #97FFFF"
                                                    value="Cập nhật" type="submit">
                                            </form>
                                        </div>
                                    </td>
                                    <td class="invert"><?php echo e($v_content->name); ?></td>
                                    <td class="invert">
                                        <?php
                                        $subtotal = $v_content->price * $v_content->qty;
                                        echo number_format($subtotal) . ' ' . 'VNĐ';
                                        ?>
                                    </td>
                                    <td class="invert">
                                        <a href="<?php echo e(URL::to('/xoa-gio-hang/' . $v_content->rowId)); ?>">
                                            <img src="<?php echo e(URL::to('/public/frontend/images/close_1.png')); ?>">
                                        </a>
                                    </td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        </tbody>
                    </table>
                    <p>Tổng tiền: <span><?php echo e(Cart::subtotal() . ' ' . 'VNĐ'); ?></span></p>
                    <p>Thuế: <?php echo e(Cart::tax() . ' ' . 'VNĐ'); ?></p>
                    <p>Thành tiền: <?php echo e(Cart::subtotal() . ' ' . 'VNĐ'); ?> </p>
                   
                </div>
            </div>
            <div class="checkout-left">
                <div class="address_form_agile mt-sm-5 mt-4">
                    <?php
                    $id_khach_hang = Session::get('id_khach_hang');
                    if ($id_khach_hang != null){
                    
                    ?>
                    <div class="checkout-right-basket">
                        <a href="<?php echo e(URL::to('/thanh-toan')); ?>">Thanh toán
                            <span class="far fa-hand-point-right"></span>
                        </a>
                    </div>
                    <?php
                    }else {
                        ?>
                    <div class="checkout-right-basket">
                        <a href="<?php echo e(URL::to('/dang-nhap-thanh-toan')); ?>" data-toggle="modal" data-target="#exampleModal">Thanh toán
                            <span class="far fa-hand-point-right"></span>
                        </a>
                    </div>
                    <?php
                    }
                     ?>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\banbdsphp\resources\views/pages/giohang/giohang.blade.php ENDPATH**/ ?>