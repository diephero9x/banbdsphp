
<?php $__env->startSection('admin_content'); ?>
<div class="panel panel-widget">
    <div class="tables">
        <h4>Liệt kê danh mục:</h4>
        <?php
				
        $message = Session::get('message');
        if($message){
            echo '<span>',$message,'</span>';
            Session::put('message', null);
        }
    ?>
        <table class="table"> 
            <thead> 
                <tr> 
                    <th>#</th> 
                    <th>Tên danh mục nhà đất</th> 
                    <th>Hiển thị</th> 
                    
                    <th></th>
                </tr> 
            </thead> 
            <tbody> 
                    <?php $__currentLoopData = $danh_sach_the_loai; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $the_loai): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        
                    
                    <tr class="active"> 
                        <th scope="row"><?php echo e($the_loai->category_id); ?></th> 
                        <td><?php echo e($the_loai->ten_loai); ?></td> 
                        <td><span class="text-ellipsis">
                            <?php
                                if($the_loai->tinh_trang==0)
                                {
                                    ?>
                                    <a href="<?php echo e(URL::to('/active-theloai/'.$the_loai->category_id)); ?>"><span class="fa-thumb-styling fa fa-thumbs-down"></span></a>
                                    </a>
                                    <?php
                                        }else {
                                    ?>
                                   
                                   <a href="<?php echo e(URL::to('/unactive-theloai/'.$the_loai->category_id)); ?>">
                                    <span class="fa-thumb-styling fa fa-thumbs-up"></span>
                                   <?php
                                }
                            ?>
                            </span></td> 
                        
                        <td>
                            <a href="<?php echo e(URL::to(('/update-nha-dat/'.$the_loai->category_id))); ?>" class="active styling-edit" ui-toggle-class="">
                                <i class="fa fa-pencil-square-o text-success text-active"></i>
                            </a>
                            
                            
                        </td>
                        <td>
                            <a onclick="return confirm('Bạn muốn xóa danh mục này không?')" href="<?php echo e(URL::to(('/delete-nha-dat/'.$the_loai->category_id))); ?>" class="active styling-delete" ui-toggle-class="">
                                <i class="fa fa-times text-danger text"></i>
                            </a>
                        </td>
                    </tr> 
                   
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody> 
        </table> 
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin_layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\banbdsphp\resources\views/admin/liet_ke_danh_muc_nha_dat.blade.php ENDPATH**/ ?>