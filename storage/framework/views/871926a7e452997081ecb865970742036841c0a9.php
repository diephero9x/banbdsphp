
<?php $__env->startSection('admin_content'); ?>
<div class="panel panel-widget">
    <div class="tables">
        <h4>Liệt kê bất động sản:</h4>
        <?php
				
        $message = Session::get('message');
        if($message){
            echo '<span>',$message,'</span>';
            Session::put('message', null);
        }
    ?>
        <table class="table"> 
            <thead> 
                <tr> 
                    <th>#</th>
                    <th>Tên bất động sản</th> 
                    <th>Giá bất động sản</th> 
                    <th>Hình ảnh</th> 
                     
                    <th>Số phòng ngủ</th> 
                    <th>Pháp lý</th> 
                    <th>Số nhà tắm</th> 
                    <th>Diện tích</th> 
                    <th>Loại bất động sản</th> 
                    <th>Khu vực</th> 
                    <th>Dự án</th> 
                    <th>Hiển thị</th> 
                    
                    <th></th>
                </tr> 
            </thead> 
            <tbody> 
                    <?php $__currentLoopData = $danh_sach_bds; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $bds): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr class="active"> 
                        <th scope="row"><?php echo e($bds->id_bds); ?></th> 
                        <td><?php echo e($bds->ten_bds); ?></td> 
                        <td><?php echo e($bds->gia_bds); ?></td> 
                        <td><img src="public/upload/batdongsan/<?php echo e($bds->hinh_anh_bds); ?>" height="200" width="200"></td> 
                         
                        <td><?php echo e($bds->so_phong_ngu); ?></td> 
                        <td><span class="text-ellipsis">
                            <?php
                                if($bds->phap_ly==0)
                                {
                                    ?>
                                    Sổ đỏ
                                    <?php
                                        }else {
                                    ?>
                                   Sổ Hồng
                                   <?php
                                }
                            ?>
                            </span></td> 

                        <td><?php echo e($bds->so_nha_tam); ?></td> 
                        <td><?php echo e($bds->dien_tich); ?></td> 
                        <td><?php echo e($bds->ten_loai); ?></td> 
                        <td><?php echo e($bds->ten_khu_vuc); ?></td> 
                        <td><?php echo e($bds->ten_du_an); ?></td> 
                        
                        <td><span class="text-ellipsis">
                            <?php
                                if($bds->tinh_trang_bds==0)
                                {
                                    ?>
                                    <a href="<?php echo e(URL::to('/active-bds/'.$bds->id_bds)); ?>"><span class="fa-thumb-styling fa fa-thumbs-down"></span></a>
                                    </a>
                                    <?php
                                        }else {
                                    ?>
                                   
                                   <a href="<?php echo e(URL::to('/unactive-bds/'.$bds->id_bds)); ?>">
                                    <span class="fa-thumb-styling fa fa-thumbs-up"></span>
                                   <?php
                                }
                            ?>
                            </span></td> 
                        <td>
                            <a href="<?php echo e(URL::to(('/update-bds/'.$bds->id_bds))); ?>" class="active styling-edit" ui-toggle-class="">
                                <i class="fa fa-pencil-square-o text-success text-active"></i>
                            </a>
                            
                            
                        </td>
                        <td>
                            <a onclick="return confirm('Bạn muốn xóa bất động sản này không?')" href="<?php echo e(URL::to(('/delete-bds/'.$bds->id_bds))); ?>" class="active styling-delete" ui-toggle-class="">
                                <i class="fa fa-times text-danger text"></i>
                            </a>
                        </td>
                    </tr> 
                   
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody> 
        </table> 
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin_layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\banbdsphp\resources\views/admin/liet_ke_bds.blade.php ENDPATH**/ ?>