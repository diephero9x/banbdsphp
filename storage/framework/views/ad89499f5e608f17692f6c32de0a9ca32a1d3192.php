<?php $__env->startSection('content'); ?>
    <div class="privacy py-sm-5 py-4">
        <div class="container py-xl-4 py-lg-2">
            <!-- tittle heading -->
            <h3 class="tittle-w3l text-center mb-lg-5 mb-sm-4 mb-3">
                <span>G</span>iỏ hàng
            </h3>
            <!-- //tittle heading -->
            <div class="checkout-right">
                
                <div class="table-responsive">

                    <form class="input-group mb-3" action="<?php echo e(URL::to('/cap-nhat-gio-hang')); ?>" method="POST">
                        <?php echo csrf_field(); ?>
                        <?php

                        $message = Session::get('message');
                        if ($message) {
                            echo '<span>', $message, '</span>';
                            Session::put('message', null);
                        }
                        ?>
                        <table class="timetable_sub">
                            <thead>
                                <tr>
                                    <th>Mã BĐS</th>
                                    <th>Hình ảnh</th>
                                    <th>Số lượng</th>
                                    <th>Tên</th>
                                    <th>Tổng Giá</th>
                                    <th>Remove</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if(Session::get('cart') == true): ?>
                                    <?php
                                        $total = 0;
                                    ?>
                                    
                                    <?php $__currentLoopData = Session::get('cart'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $cart): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php
                                            $subtotal = $cart['product_price'] * $cart['product_qty'];
                                            $total += $subtotal;
                                        ?>
                                        <tr class="rem1">
                                            <td class="invert"></td>
                                            <td class="invert-image">
                                                <a href="single.html">
                                                    <img src="<?php echo e(asset('public/upload/batdongsan/' . $cart['product_image'])); ?>"
                                                        alt="<?php echo e($cart['product_name']); ?>" width="86px" height="86px"
                                                        class="img-responsive">
                                                </a>
                                            </td>
                                            <td class="invert">
                                                <div class="input-group mb-3">
                                                    <input type="number" name="cart_qty[<?php echo e($cart['session_id']); ?>]"
                                                        min="1" style="width: 50px; height: 40px" class="form-control"
                                                        value="<?php echo e($cart['product_qty']); ?>">
                                                    <input type="hidden" name="rowID_cart" class="form-control" value="">
                                                </div>
                                            </td>
                                            <td class="invert"><?php echo e($cart['product_name']); ?></td>
                                            <td class="invert">
                                                <p><?php echo e(number_format($subtotal)); ?> VNĐ</p>
                                            </td>
                                            <td class="invert">
                                                <a href="<?php echo e(URL::to('/xoa-bds-gio-hang/' . $cart['session_id'])); ?>">
                                                    <img src="<?php echo e(URL::to('/public/frontend/images/close_1.png')); ?>">
                                                </a>
                                            </td>
                                        </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td><input class="btn btn-outline-secondary" style="background: #5fcfcf"
                                                value="Cập nhật giỏ hàng" type="submit"></td>
                                        <td><a href="<?php echo e(URL::to('/xoa-gio-hang-all')); ?>" class="btn btn-outline-secondary"
                                                style="background: #dd5d5d" value="Cập nhật giỏ hàng" type="submit">Xóa giỏ
                                                hàng</a></td>
                                        <td>
                                            <p>Tổng tiền: <?php echo e(number_format($total)); ?> VND<span></span></p>
                                            <input type="hidden" name="tongtien" value="<?php echo e(number_format($total)); ?>">
                                        </td>
                                        <td>
                                            <?php
                                             $id_khach_hang = Session::get('id_khach_hang');
                                                     if ($id_khach_hang != null){

                                            ?>
                                            <div class="checkout-right-basket">
                                                <a href="<?php echo e(URL::to('/thanh-toan')); ?>">Thanh toán
                                                    <span class="far fa-hand-point-right"></span>
                                                </a>
                                            </div>
                                            <?php
                                            }else {
                                         ?>
                                            <div class="checkout-right-basket">
                                                <a href="<?php echo e(URL::to('/dang-nhap-thanh-toan')); ?>" data-toggle="modal"
                                                    data-target="#exampleModal">Thanh toán
                                                    <span class="far fa-hand-point-right"></span>
                                                </a>
                                            </div>
                                            <?php
                                                 }
                                            ?>
                                        </td>
                                    </tr>
                                <?php else: ?>
                                    <?php
                                        echo 'Thêm sản phẩm vào giỏ hàng';
                                    ?>
                                <?php endif; ?>
                            </tbody>
                        </table>
                    </form>

                </div>
            </div>

        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\banbdsphp\resources\views/pages/giohang/cart_ajax.blade.php ENDPATH**/ ?>