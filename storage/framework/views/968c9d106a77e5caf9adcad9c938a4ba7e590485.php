
<?php $__env->startSection('content'); ?>
    <div class="ads-grid  py-sm-5 py-4">
        <div class="container py-xl-4 py-lg-2">
            <!-- tittle heading -->
            <h3 class="tittle-w3l text-center mb-lg-5 mb-sm-4 mb-3">
                <span>K</span>ết
                <span>Q</span>uả
                <span>T</span>ìm
                <span>K</span>iếm
            </h3>
            <!-- //tittle heading -->
            <div class="row">
                <!-- product left -->
                <div class="agileinfo-ads-display col-lg-9">
                    <div class="wrapper">
                        <!-- first section -->
                        <div class="product-sec1 px-sm-4 px-3 py-sm-5  py-3 mb-4">
                            <div class="row">
                                <?php $__currentLoopData = $tim_kiem_bds; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $bds): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="col-md-4 product-men mt-5">
                                        <div class="men-pro-item simpleCart_shelfItem">
                                            <div class="men-thumb-item text-center">
                                                

                                                <img src="<?php echo e(URL::to('public/upload/batdongsan/' . $bds->hinh_anh_bds)); ?>"
                                                    width="150px" height="200px" alt="">


                                                <div class="men-cart-pro">
                                                    <div class="inner-men-cart-pro">
                                                        <a href="<?php echo e(URL::to('/chi-tiet-bds/' . $bds->id_bds)); ?>"
                                                            class="link-product-add-cart">Chi Tiết</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item-info-product text-center border-top mt-4">
                                                <h4 class="pt-1">
                                                    <a
                                                        href="<?php echo e(URL::to('/chi-tiet-bds/' . $bds->id_bds)); ?>"><?php echo e($bds->ten_bds); ?></a>
                                                </h4>
                                                <div class="info-product-price my-2">
                                                    <span
                                                        class="item_price"><?php echo e(number_format($bds->gia_bds) . ' ' . 'VNĐ'); ?></span>

                                                </div>
                                                <div
                                                    class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out">
                                                    <form>
                                                        <?php echo csrf_field(); ?>
                                                        <input type="hidden" value="<?php echo e($bds->id_bds); ?>"
                                                            class="cart_product_id_<?php echo e($bds->id_bds); ?>">
                                                        <input type="hidden" value="<?php echo e($bds->ten_bds); ?>"
                                                            class="cart_product_name_<?php echo e($bds->id_bds); ?>">
                                                        <input type="hidden" value="<?php echo e($bds->hinh_anh_bds); ?>"
                                                            class="cart_product_image_<?php echo e($bds->id_bds); ?>">
                                                        <input type="hidden" value="<?php echo e($bds->gia_bds); ?>"
                                                            class="cart_product_price_<?php echo e($bds->id_bds); ?>">
                                                        <input type="hidden" value="1"
                                                            class="cart_product_qty_<?php echo e($bds->id_bds); ?>">
                                                        <input type="button" name="add-to-cart"
                                                            data-id_bds="<?php echo e($bds->id_bds); ?>" value="Thêm giỏ hàng"
                                                            class="button btn add-to-cart" />
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- //product left -->
                <!-- product right -->
                <div class="col-lg-3 mt-lg-0 mt-4 p-lg-0">
                    <div class="side-bar p-sm-4 p-3">
                        <div class="search-hotel border-bottom py-2">
                            <h3 class="agileits-sear-head mb-3">Brand</h3>
                            <form action="#" method="post">
                                <input type="search" placeholder="Search Brand..." name="search" required="">
                                <input type="submit" value=" ">
                            </form>
                            <div class="left-side py-2">
                                <ul>
                                    <li>
                                        <input type="checkbox" class="checked">
                                        <span class="span">Electronics</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" class="checked">
                                        <span class="span">ELECTRON</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" class="checked">
                                        <span class="span">Electronic</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" class="checked">
                                        <span class="span">Generic</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" class="checked">
                                        <span class="span">mono</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" class="checked">
                                        <span class="span">ACR Electronics</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" class="checked">
                                        <span class="span">NAXA Electronics</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" class="checked">
                                        <span class="span">Techno electronics</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" class="checked">
                                        <span class="span">TC Electronic</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" class="checked">
                                        <span class="span">Robodo Electronics</span>
                                    </li>
                                    <li>
                                        <input type="checkbox" class="checked">
                                        <span class="span">JJ Electronic</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- reviews -->
                        <div class="customer-rev border-bottom left-side py-2">
                            <h3 class="agileits-sear-head mb-3">Customer Review</h3>
                            <ul>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <span>5.0</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                        <span>4.0</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                        <span>3.5</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                        <span>3.0</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                        <span>2.5</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <!-- //reviews -->
                        <!-- price -->
                        <div class="range border-bottom py-2">
                            <h3 class="agileits-sear-head mb-3">Price</h3>
                            <div class="w3l-range">
                                <ul>
                                    <li>
                                        <a href="#">Under $1,000</a>
                                    </li>
                                    <li class="my-1">
                                        <a href="#">$1,000 - $5,000</a>
                                    </li>
                                    <li>
                                        <a href="#">$5,000 - $10,000</a>
                                    </li>
                                    <li class="my-1">
                                        <a href="#">$10,000 - $20,000</a>
                                    </li>
                                    <li>
                                        <a href="#">$20,000 $30,000</a>
                                    </li>
                                    <li class="mt-1">
                                        <a href="#">Over $30,000</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- //price -->
                        <!-- discounts -->
                        <div class="left-side border-bottom py-2">
                            <h3 class="agileits-sear-head mb-3">Discount</h3>
                            <ul>
                                <li>
                                    <input type="checkbox" class="checked">
                                    <span class="span">5% or More</span>
                                </li>
                                <li>
                                    <input type="checkbox" class="checked">
                                    <span class="span">10% or More</span>
                                </li>
                                <li>
                                    <input type="checkbox" class="checked">
                                    <span class="span">20% or More</span>
                                </li>
                                <li>
                                    <input type="checkbox" class="checked">
                                    <span class="span">30% or More</span>
                                </li>
                                <li>
                                    <input type="checkbox" class="checked">
                                    <span class="span">50% or More</span>
                                </li>
                                <li>
                                    <input type="checkbox" class="checked">
                                    <span class="span">60% or More</span>
                                </li>
                            </ul>
                        </div>
                        <!-- //discounts -->
                        <!-- offers -->
                        <div class="left-side border-bottom py-2">
                            <h3 class="agileits-sear-head mb-3">Offers</h3>
                            <ul>
                                <li>
                                    <input type="checkbox" class="checked">
                                    <span class="span">Exchange Offer</span>
                                </li>
                                <li>
                                    <input type="checkbox" class="checked">
                                    <span class="span">No Cost EMI</span>
                                </li>
                                <li>
                                    <input type="checkbox" class="checked">
                                    <span class="span">Special Price</span>
                                </li>
                            </ul>
                        </div>
                        <!-- //offers -->
                        <!-- delivery -->
                        <div class="left-side border-bottom py-2">
                            <h3 class="agileits-sear-head mb-3">Cash On Delivery</h3>
                            <ul>
                                <li>
                                    <input type="checkbox" class="checked">
                                    <span class="span">Eligible for Cash On Delivery</span>
                                </li>
                            </ul>
                        </div>
                        <!-- //delivery -->
                        <!-- arrivals -->
                        <div class="left-side border-bottom py-2">
                            <h3 class="agileits-sear-head mb-3">New Arrivals</h3>
                            <ul>
                                <li>
                                    <input type="checkbox" class="checked">
                                    <span class="span">Last 30 days</span>
                                </li>
                                <li>
                                    <input type="checkbox" class="checked">
                                    <span class="span">Last 90 days</span>
                                </li>
                            </ul>
                        </div>
                        <!-- //arrivals -->
                        <!-- Availability -->
                        <div class="left-side py-2">
                            <h3 class="agileits-sear-head mb-3">Availability</h3>
                            <ul>
                                <li>
                                    <input type="checkbox" class="checked">
                                    <span class="span">Exclude Out of Stock</span>
                                </li>
                            </ul>
                        </div>
                        <!-- //Availability -->
                    </div>
                </div>
                <!-- //product right -->
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\banbdsphp\resources\views/pages/batdongsan/timkiem.blade.php ENDPATH**/ ?>