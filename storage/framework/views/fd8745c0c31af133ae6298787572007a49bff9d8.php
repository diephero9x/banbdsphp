
<?php $__env->startSection('admin_content'); ?>
<div class="panel panel-widget forms-panel">
    <div class="forms">
        <div class="form-grids widget-shadow" data-example-id="basic-forms"> 
            <div class="form-title">
                <h4>Chỉnh sửa danh mục nhà đất :</h4>
            </div>
            <?php
					$message = Session::get('message');
					if($message){
						echo '<span>',$message,'</span>';
						Session::put('message', null);
					}
				?>
            <div class="form-body" >
                <?php $__currentLoopData = $cap_nhat_the_loai; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=> $cap_nhat_the_loai): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    
                
                <form action="<?php echo e(URL::to('cap-nhat-the-loai/'.$cap_nhat_the_loai->category_id)); ?>" method="post">
                    <?php echo e(csrf_field()); ?>

                     <div class="form-group">
                          <label for="exampleInputEmail1">Tên bất động sản</label> 
                          <input type="text" value="<?php echo e($cap_nhat_the_loai->ten_loai); ?>" name="ten_danh_muc" class="form-control" id="exampleInputEmail1" placeholder="Tên bất động sản"> 
                    </div> 
                    <div class="form-group">
                        <label for="exampleInputPassword1">Mô tả</label> 
                        <textarea style="resize: none"  rows="5" name="mo_ta" id="field-4" required="true" name="mo_ta_bat_dong_san" class="form-control at-required"><?php echo e($cap_nhat_the_loai->mo_ta); ?></textarea>
                    </div>
                    
                    <button type="submit" name="cap-nhat-the-loai" class="btn btn-default">Cập nhật danh mục</button>
                </form> 
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin_layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\banbdsphp\resources\views/admin/cap_nhat_danh_muc_nha_dat.blade.php ENDPATH**/ ?>