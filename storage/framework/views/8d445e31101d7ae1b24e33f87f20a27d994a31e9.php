
<?php $__env->startSection('admin_content'); ?>
<div class="panel panel-widget forms-panel">
    <div class="forms">
        <div class="form-grids widget-shadow" data-example-id="basic-forms"> 
            <div class="form-title">
                <h4>Chỉnh sửa khu vực :</h4>
            </div>
            <?php
					$message = Session::get('message');
					if($message){
						echo '<span>',$message,'</span>';
						Session::put('message', null);
					}
				?>
            <div class="form-body" >
                <?php $__currentLoopData = $cap_nhat_khu_vuc; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=> $cap_nhat_khu_vuc): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    
                
                <form action="<?php echo e(URL::to('cap-nhat-khu-vuc/'.$cap_nhat_khu_vuc->id_khu_vuc)); ?>" method="post">
                    <?php echo e(csrf_field()); ?>

                     <div class="form-group">
                          <label for="exampleInputEmail1">Tên khu vực</label> 
                          <input type="text" value="<?php echo e($cap_nhat_khu_vuc->ten_khu_vuc); ?>" name="ten_khu_vuc" class="form-control" id="exampleInputEmail1" placeholder="Tên khu vực"> 
                    </div> 
                    <button type="submit" name="cap-nhat-khu-vuc" class="btn btn-default">Cập nhật khu vực</button>
                </form> 
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin_layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\banbdsphp\resources\views/admin/cap_nhat_khu_vuc.blade.php ENDPATH**/ ?>