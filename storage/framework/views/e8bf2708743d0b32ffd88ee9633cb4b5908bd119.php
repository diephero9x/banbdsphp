
<?php $__env->startSection('admin_content'); ?>
    <div class="panel panel-widget">
        <div class="tables">
            <h4>Thông tin người mua:</h4>
            <?php
            
            $message = Session::get('message');
            if ($message) {
                echo '<span>', $message, '</span>';
                Session::put('message', null);
            }
            ?>
            <table class="table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Tên người mua</th>
                        <th>Số điện thoại</th>
                    </tr>
                </thead>
                <tbody>

                    <tr class="active">
                        <th scope="row"></th>
                        <td><?php echo e($danh_sach_don_hang_id->ten_khach_hang); ?></td>
                        <td><?php echo e($danh_sach_don_hang_id->sdt_khach_hang); ?></td>
                    </tr>


                </tbody>
            </table>
        </div>
        <br>
        <br>
        <div class="tables">
            <h4>Thông tin đặt cọc:</h4>
            <?php
            
            $message = Session::get('message');
            if ($message) {
                echo '<span>', $message, '</span>';
                Session::put('message', null);
            }
            ?>
            <table class="table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Tên người đặt cọc</th>
                        <th>Địa chỉ</th>
                        <th>Số điện thoại</th>
                    </tr>
                </thead>
                <tbody>

                    <tr class="active">
                        <th scope="row"></th>
                        <td><?php echo e($danh_sach_don_hang_id->ten_dat_coc); ?></td>
                        <td><?php echo e($danh_sach_don_hang_id->dia_chi_dat_coc); ?></td>
                        <td><?php echo e($danh_sach_don_hang_id->sdt_dat_coc); ?></td>

                    </tr>


                </tbody>
            </table>
        </div>
        <br>
        <br>
        <div class="tables">
            <h4>Chi tiết đơn hàng:</h4>
            <?php
            
            $message = Session::get('message');
            if ($message) {
                echo '<span>', $message, '</span>';
                Session::put('message', null);
            }
            ?>
            <table class="table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Tên bất động sản</th>
                        <th>Số lượng</th>
                        <th>Giá</th>
                        <th>Số lượng mua</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php $__currentLoopData = $chi_tiet_don_hang_ne; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $keysq): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr class="active">
                            <th scope="row"></th>
                            <td><?php echo e($keysq->ten_bds); ?></td>
                            <td><?php echo e($keysq->so_luong_da_mua); ?></td>
                            <td><?php echo e($keysq->gia_bds); ?></td>
                            <td><?php echo e($keysq->so_luong_da_mua); ?></td>
                            <td></td>
                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                </tbody>
            </table>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin_layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\banbdsphp\resources\views/admin/xem_don_hang.blade.php ENDPATH**/ ?>