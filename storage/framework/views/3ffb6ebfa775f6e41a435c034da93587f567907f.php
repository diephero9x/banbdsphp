
<?php $__env->startSection('content'); ?>
    <div class="container">
        <main>
            <div class="py-5 text-center">

                <h2>Checkout form</h2>
                <p class="lead">Đăng ký hoặc đăng nhập để đặt cọc</p>
            </div>

            <div class="row g-5">
                
                <div class="col-md-7 col-lg-8">
                    <h4 class="mb-3">Thông tin đặt cọc</h4>
                    <?php if(Session::get('cart') == true): ?>
                    <form action="<?php echo e(URL::to('/luu-thanh-toan-dat-coc')); ?>" method="post" class="creditly-card-form agileinfo_form">
                        <?php echo e(csrf_field()); ?>

                        <div class="creditly-wrapper wthree, w3_agileits_wrapper">
                            <div class="information-wrapper">
                                <div class="first-row">
                                    <div class="controls form-group">
                                        <input class="billing-address-name form-control" type="text" name="ten_dat_coc"
                                            placeholder="Họ và tên" required="">
                                    </div>
                                    <div class="w3_agileits_card_number_grids">
                                        <div class="w3_agileits_card_number_grid_left form-group">
                                            <div class="controls">
                                                <input type="text" class="form-control" placeholder="Số điện thoại"
                                                    name="sdt_dat_coc" required="">
                                            </div>
                                        </div>
                                        <div class="w3_agileits_card_number_grid_right form-group">
                                            <div class="controls">
                                                <input type="text" class="form-control" placeholder="Địa chỉ"
                                                    name="dia_chi_dat_coc" required="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="controls form-group">
                                        <input type="email" class="form-control" placeholder="Email" name="email_dat_coc"
                                            required="">
                                    </div>
                                    <div class="controls form-group">

                                        <textarea name="ghi_chu_dat_coc" placeholder="Ghi chú đặt cọc" class="form-control"></textarea>
                                    </div>
                                </div>
                                <button class="submit check_out btn" type="submit" name="gui_thong_tin_dat_coc">Đặt cọc</button>
                            </div>
                        </div>
                    </form>
                    <?php endif; ?>
                </div>
            </div>
        </main>

        <footer class="my-5 pt-5 text-muted text-center text-small">
            <p class="mb-1">© 2017–2021 Company Name</p>
            <ul class="list-inline">
                <li class="list-inline-item"><a href="#">Privacy</a></li>
                <li class="list-inline-item"><a href="#">Terms</a></li>
                <li class="list-inline-item"><a href="#">Support</a></li>
            </ul>
        </footer>
    </div>
    <script src="/docs/5.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
    </script>
    <script src="form-validation.js"></script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\banbdsphp\resources\views/pages/thanhtoan/thanh_toan.blade.php ENDPATH**/ ?>