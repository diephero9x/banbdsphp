<?php

namespace App\Http\Controllers;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;

class KhuVucController extends Controller
{
      //Thêm dự án
      public function KiemTraLogin(){
        $admin_id = Session::get('admin_id');
        if ($admin_id) {
            return Redirect::to('dashboard');
        }else{
            return Redirect::to('admin')->send();
        }
    }
      public function them(){
        $this->KiemTraLogin();
        return view('admin.them_khu_vuc');
    }
    public function lietke(){
        $this->KiemTraLogin();
        $danh_sach_khu_vuc=DB::table('tbl_khu_vuc')->get(); // chiếu tới table tbl_khu_vuc và lấy ra tất cả dữ liệu bên trong nó
        $quan_ly_khu_vuc = view('admin.liet_ke_khu_vuc')->with('danh_sach_khu_vuc', $danh_sach_khu_vuc);
        return view('admin_layout')->with('admin.liet_ke_khu_vuc', $quan_ly_khu_vuc);
    }
    public function luu(Request $request){
        $data = array();
        $data['ten_khu_vuc'] = $request->ten_khu_vuc; //= Chieens
        $data['tinh_trang_khu_vuc'] = $request->tinh_trang_khu_vuc; //=1
        
        DB::table('tbl_khu_vuc')->insert($data);
        Session::put('message','Thêm khu vực thành công');//
        return Redirect::to('all-khu-vuc');
    }
    //$du_an_id lay tu ben web.php
    public function active_khuvuc($id_khu_vuc){
        DB::table('tbl_khu_vuc')->where('id_khu_vuc', $id_khu_vuc)->update(['tinh_trang_khu_vuc'=>1]);
        Session::put('message',' Kích hoạt khu vực thành công');
        return Redirect::to('all-khu-vuc');
    }
    public function unactive_khuvuc($id_khu_vuc){
        DB::table('tbl_khu_vuc')->where('id_khu_vuc', $id_khu_vuc)->update(['tinh_trang_khu_vuc'=>0]);
        Session::put('message','Không kích hoạt khu vực thành công');
        return Redirect::to('all-khu-vuc');
    }

    //Sửa danh mục
    public function sua($id_khu_vuc){
        $this->KiemTraLogin();
        
        // chỉ lấy ra dữ liệu ứng với category id
        $cap_nhat_khu_vuc=DB::table('tbl_khu_vuc')->where('id_khu_vuc', $id_khu_vuc)->get();
        $quan_ly_khu_vuc = view('admin.cap_nhat_khu_vuc')->with('cap_nhat_khu_vuc', $cap_nhat_khu_vuc);
        return view('admin_layout')->with('admin.cap_nhat_khu_vuc', $quan_ly_khu_vuc);
    }
    public function cap_nhat(Request $request,$id_khu_vuc){
        $data = array();
        $data['ten_khu_vuc'] = $request->ten_khu_vuc;
       
        DB::table('tbl_khu_vuc')->where('id_khu_vuc', $id_khu_vuc)->update($data);
        Session::put('message','Cập nhật khu vực thành công');
        return Redirect::to('all-khu-vuc');
    }   
    // xóa danh mục
    public function xoa($id_khu_vuc){
        DB::table('tbl_khu_vuc')->where('id_khu_vuc', $id_khu_vuc)->delete();
        Session::put('message',' Xóa khu vực thành công');
        return Redirect::to('all-khu-vuc');
    }
    //
    public function danh_sach_bds_theo_khu_vuc($id_khu_vuc)
    {
        $danh_sach_the_loai=DB::table('tbl_theloai')->where('tinh_trang','1')->orderby('category_id', 'desc')->get();
        $danh_sach_du_an=DB::table('tbl_du_an')->where('tinh_trang_du_an', '1')->orderby('id_du_an', 'desc')->get();
        $danh_sach_khu_vuc=DB::table('tbl_khu_vuc')->where('tinh_trang_khu_vuc', '1')->orderby('id_khu_vuc', 'desc')->get();
        $danh_sach_bds = DB::table('tbl_bat_dong_san')->join('tbl_khu_vuc','tbl_bat_dong_san.id_khu_vuc','=','tbl_khu_vuc.id_khu_vuc')
        ->where('tbl_bat_dong_san.id_khu_vuc',$id_khu_vuc)->get();
        $ten_khu_vuc=DB::table('tbl_khu_vuc')->where('tinh_trang_khu_vuc', '1')->where('id_khu_vuc', $id_khu_vuc)->limit(1)->get();
        return view('pages.loaibds.danh_sach_bds_theo_khu_vuc')->with('loaibds', $danh_sach_the_loai)
        ->with('duan', $danh_sach_du_an)
        ->with('khuvuc', $danh_sach_khu_vuc)
        ->with('bds', $danh_sach_bds)
        ->with('tenkv', $ten_khu_vuc);
    }
}
