<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
class DanhMucSanPham extends Controller
{
    public function KiemTraLogin(){
        $admin_id = Session::get('admin_id');
        if ($admin_id) {
            return Redirect::to('dashboard');
        }else{
            return Redirect::to('admin')->send();
        }
    }
    //Thêm danh mục
    public function them(){
        $this->KiemTraLogin();
        return view('admin.them_danh_muc_nha_dat');
    }
    public function lietke(){
        $this->KiemTraLogin();
        $danh_sach_the_loai=DB::table('tbl_theloai')->orderby('category_id', 'desc')->get();
        $quan_ly_danh_muc = view('admin.liet_ke_danh_muc_nha_dat')->with('danh_sach_the_loai', $danh_sach_the_loai);
        return view('admin_layout')->with('admin.liet_ke_danh_muc_nha_dat', $quan_ly_danh_muc);
    }
    public function luu(Request $request){
        $data = array();
        $data['ten_loai'] = $request->ten_danh_muc;
        $data['mo_ta'] = $request->mo_ta;
        $data['tinh_trang'] = $request->tinh_trang;
        
        DB::table('tbl_theloai')->insert($data);
        Session::put('message','Thêm danh mục thành công');
        return Redirect::to('all-nha-dat');
    }
    //$category_id lay tu ben web.php
    public function active_theloai($category_id){
        DB::table('tbl_theloai')->where('category_id', $category_id)->update(['tinh_trang'=>1]);
        Session::put('message',' kích hoạt danh mục thành công');
        return Redirect::to('all-nha-dat');
    }
    public function unactive_theloai($category_id){
        DB::table('tbl_theloai')->where('category_id', $category_id)->update(['tinh_trang'=>0]);
        Session::put('message','Không kích hoạt danh mục thành công');
        return Redirect::to('all-nha-dat');
    }

    //Sửa danh mục
    public function sua($category_id){
        $this->KiemTraLogin();
        // chỉ lấy ra dữ liệu ứng với category id
        $cap_nhat_the_loai=DB::table('tbl_theloai')->where('category_id', $category_id)->get();
        $quan_ly_danh_muc = view('admin.cap_nhat_danh_muc_nha_dat')->with('cap_nhat_the_loai', $cap_nhat_the_loai);
        return view('admin_layout')->with('admin.cap_nhat_danh_muc_nha_dat', $quan_ly_danh_muc);
    }
    public function cap_nhat(Request $request,$category_id){
        $data = array();
        $data['ten_loai'] = $request->ten_danh_muc;
        $data['mo_ta'] = $request->mo_ta;
        
        DB::table('tbl_theloai')->where('category_id', $category_id)->update($data);
        Session::put('message','Cập nhật danh mục thành công');
        return Redirect::to('all-nha-dat');
    }   
    // xóa danh mục
    public function xoa($category_id){
        $this->KiemTraLogin();
        DB::table('tbl_theloai')->where('category_id', $category_id)->delete();
        Session::put('message',' Xóa danh mục thành công');
        return Redirect::to('all-nha-dat');
    }
    public function danh_sach_bds_theo_loai($category_id)
    {
        $danh_sach_the_loai=DB::table('tbl_theloai')->where('tinh_trang','1')->orderby('category_id', 'desc')->get();
        $danh_sach_du_an=DB::table('tbl_du_an')->where('tinh_trang_du_an', '1')->orderby('id_du_an', 'desc')->get();
        $danh_sach_khu_vuc=DB::table('tbl_khu_vuc')->where('tinh_trang_khu_vuc', '1')->orderby('id_khu_vuc', 'desc')->get();
        $danh_sach_bds = DB::table('tbl_bat_dong_san')->join('tbl_theloai','tbl_bat_dong_san.category_id','=','tbl_theloai.category_id')
        ->where('tbl_bat_dong_san.category_id',$category_id)->where('tinh_trang_bds', '1')->get();
        $ten_loai=DB::table('tbl_theloai')->where('tinh_trang', '1')->where('category_id', $category_id)->limit(1)->get();
        return view('pages.loaibds.danh_sach_bds_theo_loai')->with('loaibds', $danh_sach_the_loai)
        ->with('duan', $danh_sach_du_an)
        ->with('khuvuc', $danh_sach_khu_vuc)
        ->with('bds', $danh_sach_bds)
        ->with('tenloai', $ten_loai);
    }
    //Show bất động sản theo loại
    
}
