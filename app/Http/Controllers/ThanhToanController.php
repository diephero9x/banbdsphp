<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Session;
use Cart;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;

class ThanhToanController extends Controller
{
    public function KiemTraLogin(){
        $admin_id = Session::get('admin_id');
        if ($admin_id) {
            return Redirect::to('dashboard');
        }else{
            return Redirect::to('admin')->send();
        }
    }
    //
    public function dang_nhap_thanh_toan(){
        $danh_sach_the_loai=DB::table('tbl_theloai')->where('tinh_trang','1')->orderby('category_id', 'desc')->get();
        $danh_sach_du_an=DB::table('tbl_du_an')->where('tinh_trang_du_an', '1')->orderby('id_du_an', 'desc')->get();
        $danh_sach_khu_vuc=DB::table('tbl_khu_vuc')->where('tinh_trang_khu_vuc', '1')->orderby('id_khu_vuc', 'desc')->get();
        return view('pages.thanhtoan.dang_nhap_thanh_toan')->with('loaibds', $danh_sach_the_loai)
        ->with('duan', $danh_sach_du_an)
        ->with('khuvuc', $danh_sach_khu_vuc);
    }
    public function dang_ky_khach_hang(Request $request){
        $data=array();
        $data['ten_khach_hang'] = $request->ten_khach_hang;
        $data['email_khach_hang'] = $request->email_khach_hang;
        $data['password_khach_hang'] = md5($request->password_khach_hang);
        $data['sdt_khach_hang'] = $request->sdt_khach_hang;
        $data['dia_chi_khach_hang'] = $request->dia_chi_khach_hang;
        
        $id_khach_hang = DB::table('tbl_khach_hang')->insertGetId($data);  
        Session::put('id_khach_hang', $id_khach_hang);
        Session::put('ten_khach_hang', $request->ten_khach_hang);
        return Redirect('/thanh-toan');
    }

    public function thanh_toan(){
        $danh_sach_the_loai=DB::table('tbl_theloai')->where('tinh_trang','1')->orderby('category_id', 'desc')->get();
        $danh_sach_du_an=DB::table('tbl_du_an')->where('tinh_trang_du_an', '1')->orderby('id_du_an', 'desc')->get();
        $danh_sach_khu_vuc=DB::table('tbl_khu_vuc')->where('tinh_trang_khu_vuc', '1')->orderby('id_khu_vuc', 'desc')->get();
        return view('pages.thanhtoan.thanh_toan')->with('loaibds', $danh_sach_the_loai)
        ->with('duan', $danh_sach_du_an)
        ->with('khuvuc', $danh_sach_khu_vuc);
    }

    public function luu_thanh_toan_dat_coc(Request $request){
        $data=array();
        $data['ten_dat_coc'] = $request->ten_dat_coc;
        $data['dia_chi_dat_coc'] = $request->dia_chi_dat_coc;
        $data['sdt_dat_coc'] = $request->sdt_dat_coc;
        $data['email_dat_coc'] = $request->email_dat_coc;
        $data['ghi_chu_dat_coc'] = $request->ghi_chu_dat_coc;
        
        $id_dat_coc = DB::table('tbl_thong_tin_dat_coc')->insertGetId($data);   
        Session::put('id_dat_coc', $id_dat_coc);
        return Redirect('/dat-coc');
    }
    public function tra_tien_dat_coc(Request $request){
           //
           $data = $request->all();
           $cart = Session::get('cart');
           $tong_tien =0;
           foreach(Session::get('cart') as $key => $tinhtoan)
           {
            $tong_don_gia = $tinhtoan['product_price'] * $tinhtoan['product_qty'];
            $tong_tien += $tong_don_gia;
           }
           $danh_sach_the_loai=DB::table('tbl_theloai')->where('tinh_trang','1')->orderby('category_id', 'desc')->get();
           $danh_sach_du_an=DB::table('tbl_du_an')->where('tinh_trang_du_an', '1')->orderby('id_du_an', 'desc')->get();
           $danh_sach_khu_vuc=DB::table('tbl_khu_vuc')->where('tinh_trang_khu_vuc', '1')->orderby('id_khu_vuc', 'desc')->get();
           //
        $data=array();
        $data['hinh_thuc_dat_coc'] = $request->hinh_thuc_dat_coc;
        $data['tinh_trang_dat_coc'] = 'Đang chờ xử lý';
        $id_hinh_thuc = DB::table('tbl_dat_coc')->insertGetId($data);
       // thêm đơn hàng
        $order_data=array();
        $order_data['id_khach_hang'] = Session::get('id_khach_hang');
        $order_data['id_dat_coc'] = Session::get('id_dat_coc');
        $order_data['id_hinh_thuc'] = $id_hinh_thuc;
        $order_data['tong_don_hang'] = $tong_tien;
        $order_data['trang_thai_don_hang'] = 'Đang chờ xử lý';
        $id_don_hang = DB::table('tbl_don_hang')->insertGetId($order_data);
        // theem chi tiết đơn hàng
        
        foreach($cart as $v_content)
        {
            $order_d_data=array();
            $order_d_data['id_don_hang'] = $id_don_hang;
            $order_d_data['id_bds'] = $v_content['product_id'];
            $order_d_data['ten_bds'] = $v_content['product_name'];
            $order_d_data['gia_bds'] = $v_content['product_price'];
            $order_d_data['so_luong_da_mua'] = $v_content['product_qty'];
            DB::table('tbl_chi_tiet_don_hang')->insertGetId($order_d_data);
        }
        if( $data['hinh_thuc_dat_coc']==1){
            echo 'Thanh toan the ATM';
        }elseif($data['hinh_thuc_dat_coc']==0){
            Session::forget('cart');
           return view('pages.thanhtoan.thongbao_tienmat')->with('loaibds', $danh_sach_the_loai)
           ->with('duan', $danh_sach_du_an)
           ->with('khuvuc', $danh_sach_khu_vuc);
        }

        // return Redirect('/payment');
    }
    public function dat_coc(){
        $danh_sach_the_loai=DB::table('tbl_theloai')->where('tinh_trang','1')->orderby('category_id', 'desc')->get();
        $danh_sach_du_an=DB::table('tbl_du_an')->where('tinh_trang_du_an', '1')->orderby('id_du_an', 'desc')->get();
        $danh_sach_khu_vuc=DB::table('tbl_khu_vuc')->where('tinh_trang_khu_vuc', '1')->orderby('id_khu_vuc', 'desc')->get();
        return view('pages.thanhtoan.dat_coc')->with('loaibds', $danh_sach_the_loai)
        ->with('duan', $danh_sach_du_an)
        ->with('khuvuc', $danh_sach_khu_vuc);
    }

    public function dang_xuat_khach_hang(){
        Session::flush();
        return Redirect('/trang-chu');
    }

    public function dang_nhap_khach_hang(Request $request){
        $email =  $request->email_khach_hang_login;
        $password = md5($request->password_khach_hang_login);
        $ketqua= DB::table('tbl_khach_hang')->where('email_khach_hang', $email)->where('password_khach_hang', $password)->first();
        if($ketqua)
        {
            Session::put('id_khach_hang', $ketqua->id_khach_hang);
            return Redirect::to('/thanh-toan');
        }
        else{
           
            return Redirect::to('/trang-chu');
        }
       
    }

    public function quan_ly_don_hang(){
        $this->KiemTraLogin();
        //join vào một table khác
        $danh_sach_don_hang=DB::table('tbl_don_hang')
        ->join('tbl_khach_hang','tbl_don_hang.id_khach_hang',"=",'tbl_khach_hang.id_khach_hang')
        ->select('tbl_don_hang.*','tbl_khach_hang.ten_khach_hang')
        ->orderby('tbl_don_hang.id_don_hang','desc')->get();
        $quan_ly_don_hang = view('admin.quanly_don_hang')->with('danh_sach_don_hang', $danh_sach_don_hang);
        return view('admin_layout')->with('admin.quanly_don_hang', $quan_ly_don_hang);
    }

    public function xem_don_hang($id_don_hang){
        $this->KiemTraLogin();
        //join vào một table khác
        
        $danh_sach_don_hang_id=DB::table('tbl_don_hang')
        ->join('tbl_khach_hang','tbl_don_hang.id_khach_hang',"=",'tbl_khach_hang.id_khach_hang')
        ->join('tbl_thong_tin_dat_coc','tbl_don_hang.id_dat_coc',"=",'tbl_thong_tin_dat_coc.id_dat_coc')
        ->join('tbl_chi_tiet_don_hang','tbl_don_hang.id_don_hang',"=",'tbl_chi_tiet_don_hang.id_don_hang')
        ->select('tbl_don_hang.*','tbl_khach_hang.*', 'tbl_thong_tin_dat_coc.*','tbl_chi_tiet_don_hang.*')->first();
        $chi_tiet_don_hang_ne = DB::table('tbl_chi_tiet_don_hang')->where('tbl_chi_tiet_don_hang.id_don_hang',$id_don_hang)->get();
        $quan_ly_don_hang_id = view('admin.xem_don_hang')->with('danh_sach_don_hang_id', $danh_sach_don_hang_id)->with('chi_tiet_don_hang_ne', $chi_tiet_don_hang_ne);
        return view('admin_layout')->with('admin.xem_don_hang', $quan_ly_don_hang_id);
        
    }
}
