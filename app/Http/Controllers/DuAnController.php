<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;

class DuAnController extends Controller
{
    public function KiemTraLogin(){
        $admin_id = Session::get('admin_id');
        if ($admin_id) {
            return Redirect::to('dashboard');
        }else{
            return Redirect::to('admin')->send();
        }
    }
    //Thêm dự án
    public function them(){
        $this->KiemTraLogin();
        return view('admin.them_du_an');
    }
    public function lietke(){
        $this->KiemTraLogin();
        $danh_sach_du_an=DB::table('tbl_du_an')->orderby('id_du_an', 'desc')->get();
        $quan_ly_du_an = view('admin.liet_ke_du_an')->with('danh_sach_du_an', $danh_sach_du_an);
        return view('admin_layout')->with('admin.liet_ke_du_an', $quan_ly_du_an);
    }
    public function luu(Request $request){
        $data = array();
        $data['ten_du_an'] = $request->ten_du_an;
        $data['mo_ta_du_an'] = $request->mo_ta;
        $data['tinh_trang_du_an'] = $request->tinh_trang_du_an;

        DB::table('tbl_du_an')->insert($data);
        Session::put('message','Thêm dự án thành công');
        return Redirect::to('all-du-an');
    }
    //$du_an_id lay tu ben web.php
    public function active_duan($id_du_an){
        DB::table('tbl_du_an')->where('id_du_an', $id_du_an)->update(['tinh_trang_du_an'=>1]);
        Session::put('message',' Kích hoạt danh mục thành công');
        return Redirect::to('all-du-an');
    }
    public function unactive_duan($id_du_an){
        DB::table('tbl_du_an')->where('id_du_an', $id_du_an)->update(['tinh_trang_du_an'=>0]);
        Session::put('message','Không kích hoạt dự án thành công');
        return Redirect::to('all-du-an');
    }

    //Sửa du an
    public function sua($id_du_an){
        $this->KiemTraLogin();
        // chỉ lấy ra dữ liệu ứng với id du an
        $cap_nhat_du_an=DB::table('tbl_du_an')->where('id_du_an', $id_du_an)->get();
        $quan_ly_du_an = view('admin.cap_nhat_du_an')->with('cap_nhat_du_an', $cap_nhat_du_an);
        return view('admin_layout')->with('admin.cap_nhat_du_an', $quan_ly_du_an);
    }
    public function cap_nhat(Request $request,$id_du_an){
        $data = array();
        $data['ten_du_an'] = $request->ten_du_an;
        $data['mo_ta_du_an'] = $request->mo_ta_du_an;

        DB::table('tbl_du_an')->where('id_du_an', $id_du_an)->update($data);
        Session::put('message','Cập nhật danh mục thành công');
        return Redirect::to('all-du-an');
    }
    // xóa danh mục
    public function xoa($id_du_an){
        DB::table('tbl_du_an')->where('id_du_an', $id_du_an)->delete();
        Session::put('message',' Xóa dự án thành công');
        return Redirect::to('all-du-an');
    }
    //
    public function danh_sach_bds_theo_du_an($id_du_an)
    {
        $danh_sach_the_loai=DB::table('tbl_theloai')->where('tinh_trang','1')->orderby('category_id', 'desc')->get();
        $danh_sach_du_an=DB::table('tbl_du_an')->where('tinh_trang_du_an', '1')->orderby('id_du_an', 'desc')->get();
        $danh_sach_khu_vuc=DB::table('tbl_khu_vuc')->where('tinh_trang_khu_vuc', '1')->orderby('id_khu_vuc', 'desc')->get();
        $danh_sach_bds = DB::table('tbl_bat_dong_san')->join('tbl_du_an','tbl_bat_dong_san.id_du_an','=','tbl_du_an.id_du_an')
        ->where('tbl_bat_dong_san.id_du_an',$id_du_an)->get();
        $ten_du_an=DB::table('tbl_du_an')->where('tinh_trang_du_an', '1')->where('id_du_an', $id_du_an)->limit(1)->get();
        return view('pages.loaibds.danh_sach_bds_theo_du_an')->with('loaibds', $danh_sach_the_loai)
        ->with('duan', $danh_sach_du_an)
        ->with('khuvuc', $danh_sach_khu_vuc)
        ->with('bds', $danh_sach_bds)
        ->with('tenduan', $ten_du_an);
    }
}
