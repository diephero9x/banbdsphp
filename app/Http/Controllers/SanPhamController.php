<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;

class SanPhamController extends Controller
{
    public function KiemTraLogin(){
        $admin_id = Session::get('admin_id');
        if ($admin_id) {
            return Redirect::to('dashboard');
        }else{
            return Redirect::to('admin')->send();
        }
    }
     //Thêm dự án
     public function them(){
        $this->KiemTraLogin();
         $ten_loai= DB::table('tbl_theloai')->orderby('category_id','desc')->get();
         $ten_khu_vuc= DB::table('tbl_khu_vuc')->orderby('id_khu_vuc','desc')->get();
         $ten_du_an= DB::table('tbl_du_an')->orderby('id_du_an','desc')->get();
        return view('admin.them_bds')->with('ten_khu_vuc',$ten_khu_vuc)->with('ten_loai',$ten_loai)->with('ten_du_an',$ten_du_an);
    }
    public function lietke(){
        $this->KiemTraLogin();
        //join vào một table khác
        $danh_sach_bds=DB::table('tbl_bat_dong_san')
        ->join('tbl_theloai','tbl_theloai.category_id',"=",'tbl_bat_dong_san.category_id')
        ->join('tbl_du_an','tbl_du_an.id_du_an',"=",'tbl_bat_dong_san.id_du_an')
        ->join('tbl_khu_vuc','tbl_khu_vuc.id_khu_vuc',"=",'tbl_bat_dong_san.id_khu_vuc')->orderby('id_bds','desc')->get();
        $quan_ly_bds = view('admin.liet_ke_bds')->with('danh_sach_bds', $danh_sach_bds);
        return view('admin_layout')->with('admin.liet_ke_bds', $quan_ly_bds);
    }
    public function luu(Request $request){
        $data = array();
        $data['ten_bds'] = $request->ten_bds;
        $data['category_id'] = $request->loai_bds;
        $data['id_du_an'] = $request->du_an_bds;
        $data['id_khu_vuc'] = $request->khu_vuc_bds;
        
        $data['gia_bds'] = $request->gia_bds;
        $data['gia_bds'] = $request->gia_bds;
        $data['so_phong_ngu'] = $request->so_phong_ngu;
        $data['phap_ly'] = $request->phap_ly;
        $data['so_nha_tam'] = $request->so_nha_tam;
        $data['dien_tich'] = $request->dien_tich;
        $data['mo_ta_bds'] = $request->mo_ta;

        $data['tinh_trang_bds'] = $request->tinh_trang_bds;
        $get_img=$request->file('hinh_anh_bds');
        $get_img2=$request->file('hinh_anh_bds2');
        $get_img3=$request->file('hinh_anh_bds3');
        if($get_img || $get_img1 || $get_img2){
            $get_name_img = $get_img->getClientOriginalName();
            $name_img = current(explode('.', $get_name_img));
            $new_image = $name_img.rand(0,99).'.'.$get_img->getClientOriginalExtension();
            $get_img->move('public/upload/batdongsan', $new_image);

            $get_name_img2= $get_img2->getClientOriginalName();
            $name_img2 = current(explode('.', $get_name_img2));
            $new_image2 = $name_img2.rand(0,99).'.'.$get_img2->getClientOriginalExtension();
            $get_img2->move('public/upload/batdongsan', $new_image2);

            $get_name_img3 = $get_img3->getClientOriginalName();
            $name_img3 = current(explode('.', $get_name_img3));
            $new_image3 = $name_img3.rand(0,99).'.'.$get_img3->getClientOriginalExtension();
            $get_img3->move('public/upload/batdongsan', $new_image3);

            $data['hinh_anh_bds'] = $new_image;
            $data['hinh_anh_bds2'] = $new_image2;
            $data['hinh_anh_bds3'] = $new_image3;
            DB::table('tbl_bat_dong_san')->insert($data);
            Session::put('message','Thêm dự án thành công');
            return Redirect::to('all-bds');
        }
        $data['hinh_anh_bds'] = '';
        $data['hinh_anh_bds2'] = '';
        $data['hinh_anh_bds3'] = '';
        DB::table('tbl_bat_dong_san')->insert($data);
        Session::put('message','Thêm dự án thành công');
        return Redirect::to('all-bds');
    }
    //$du_an_id lay tu ben web.php
    public function active_bds($id_bds){
        DB::table('tbl_bat_dong_san')->where('id_bds', $id_bds)->update(['tinh_trang_bds'=>1]);
        Session::put('message',' Kích hoạt bất động sản thành công');
        return Redirect::to('all-bds');
    }
    public function unactive_bds($id_bds){
        DB::table('tbl_bat_dong_san')->where('id_bds', $id_bds)->update(['tinh_trang_bds'=>0]);
        Session::put('message','Không kích hoạt bất động sản thành công');
        return Redirect::to('all-bds');
    }

    //Sửa danh mục
    public function sua($id_bds){
        $this->KiemTraLogin();
        // chỉ lấy ra dữ liệu ứng với category id
        $ten_loai= DB::table('tbl_theloai')->orderby('category_id','desc')->get();
        $ten_khu_vuc= DB::table('tbl_khu_vuc')->orderby('id_khu_vuc','desc')->get();
        $ten_du_an= DB::table('tbl_du_an')->orderby('id_du_an','desc')->get();

        $cap_nhat_bds=DB::table('tbl_bat_dong_san')->where('id_bds', $id_bds)->get();
        $quan_ly_bds = view('admin.cap_nhat_bds')->with('cap_nhat_bds', $cap_nhat_bds)
        ->with('ten_khu_vuc',$ten_khu_vuc)->with('ten_loai',$ten_loai)->with('ten_du_an',$ten_du_an);
        return view('admin_layout')->with('admin.cap_nhat_du_an', $quan_ly_bds);
    }
    public function cap_nhat(Request $request,$id_bds){
        $data = array();
        $data['ten_bds'] = $request->ten_bds;
        $data['category_id'] = $request->loai_bds;
        $data['id_du_an'] = $request->du_an_bds;
        $data['id_khu_vuc'] = $request->khu_vuc_bds;
        
        $data['gia_bds'] = $request->gia_bds;
        $data['gia_bds'] = $request->gia_bds;
        $data['so_phong_ngu'] = $request->so_phong_ngu;
        $data['phap_ly'] = $request->phap_ly;
        $data['so_nha_tam'] = $request->so_nha_tam;
        $data['dien_tich'] = $request->dien_tich;
        $data['mo_ta_bds'] = $request->mo_ta;
        $get_img=$request->file('hinh_anh_bds');
        $get_img2=$request->file('hinh_anh_bds2');
        $get_img3=$request->file('hinh_anh_bds3');
        if($get_img || $get_img2 || $get_img3 ){
            $get_name_img = $get_img->getClientOriginalName();
            $name_img = current(explode('.', $get_name_img));
            $new_image = $name_img.rand(0,99).'.'.$get_img->getClientOriginalExtension();
            $get_img->move('public/upload/batdongsan', $new_image);

            $get_name_img2 = $get_img2->getClientOriginalName();
            $name_img2 = current(explode('.', $get_name_img2));
            $new_image2 = $name_img2.rand(0,99).'.'.$get_img2->getClientOriginalExtension();
            $get_img2->move('public/upload/batdongsan', $new_image2);

            $get_name_img3 = $get_img3->getClientOriginalName();
            $name_img3 = current(explode('.', $get_name_img3));
            $new_image3 = $name_img3.rand(0,99).'.'.$get_img3->getClientOriginalExtension();
            $get_img3->move('public/upload/batdongsan', $new_image3);

            $data['hinh_anh_bds'] = $new_image;
            $data['hinh_anh_bds2'] = $new_image2;
            $data['hinh_anh_bds3'] = $new_image3;
            DB::table('tbl_bat_dong_san')->where('id_bds', $id_bds)->update($data);
            Session::put('message','Cập nhật thành công');
            return Redirect::to('all-bds');
        }
        DB::table('tbl_bat_dong_san')->where('id_bds', $id_bds)->update($data);
        Session::put('message','Cập nhật thành công');
        return Redirect::to('all-bds');
    }   
    // xóa danh mục
    public function xoa($id_bds){
        $this->KiemTraLogin();
        DB::table('tbl_bat_dong_san')->where('id_bds', $id_bds)->delete();
        Session::put('message',' Xóa dự án thành công');
        return Redirect::to('all-bds');
    }

    //Chi tiết bđs
    public function chitietbds($id_bds)
    {
        $danh_sach_the_loai=DB::table('tbl_theloai')->where('tinh_trang','1')->orderby('category_id', 'desc')->get();
        $danh_sach_du_an=DB::table('tbl_du_an')->where('tinh_trang_du_an', '1')->orderby('id_du_an', 'desc')->get();
        $danh_sach_khu_vuc=DB::table('tbl_khu_vuc')->where('tinh_trang_khu_vuc', '1')->orderby('id_khu_vuc', 'desc')->get();

        $chi_tiet_bds=DB::table('tbl_bat_dong_san')
        ->join('tbl_theloai','tbl_theloai.category_id',"=",'tbl_bat_dong_san.category_id')
        ->join('tbl_du_an','tbl_du_an.id_du_an',"=",'tbl_bat_dong_san.id_du_an')
        ->join('tbl_khu_vuc','tbl_khu_vuc.id_khu_vuc',"=",'tbl_bat_dong_san.id_khu_vuc')->orderby('id_bds','desc')->where('tbl_bat_dong_san.id_bds',$id_bds)->get();

        return view('pages.batdongsan.chi_tiet_bds')->with('loaibds', $danh_sach_the_loai)
        ->with('duan', $danh_sach_du_an)
        ->with('khuvuc', $danh_sach_khu_vuc)
        ->with('chitietbds', $chi_tiet_bds);
    }
}
