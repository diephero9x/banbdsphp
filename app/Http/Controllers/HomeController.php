<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;

class HomeController extends Controller
{
    public function index(){
        
        $meta_title = "Trang chủ" ;
        $danh_sach_the_loai=DB::table('tbl_theloai')->where('tinh_trang','1')->orderby('category_id', 'desc')->get();
        $danh_sach_du_an=DB::table('tbl_du_an')->where('tinh_trang_du_an', '1')->orderby('id_du_an', 'desc')->get();
        $danh_sach_khu_vuc=DB::table('tbl_khu_vuc')->where('tinh_trang_khu_vuc', '1')->orderby('id_khu_vuc', 'desc')->get();
        $danh_sach_canho=DB::table('tbl_bat_dong_san')->where('tinh_trang_bds', '1')->where('category_id','1')->orderby('id_bds', 'desc')->limit(3)->get();
        $danh_sach_nha=DB::table('tbl_bat_dong_san')->where('tinh_trang_bds', '1')->where('category_id','3')->orderby('id_bds', 'desc')->limit(3)->get();
        $danh_sach_dat=DB::table('tbl_bat_dong_san')->where('tinh_trang_bds', '1')->where('category_id','2')->orderby('id_bds', 'desc')->limit(3)->get();
        return view('pages.home')
        ->with('loaibds', $danh_sach_the_loai)
        ->with('duan', $danh_sach_du_an)
        ->with('khuvuc', $danh_sach_khu_vuc)
        ->with('canho', $danh_sach_canho)
        ->with('nha', $danh_sach_nha)
        ->with('dat', $danh_sach_dat)
        ->with('meta_title', $meta_title);
    }
    public function tim_kiem(Request $request){
        $key_words = $request->key_works_submit;
        $danh_sach_the_loai=DB::table('tbl_theloai')->where('tinh_trang','1')->orderby('category_id', 'desc')->get();
        $danh_sach_du_an=DB::table('tbl_du_an')->where('tinh_trang_du_an', '1')->orderby('id_du_an', 'desc')->get();
        $danh_sach_khu_vuc=DB::table('tbl_khu_vuc')->where('tinh_trang_khu_vuc', '1')->orderby('id_khu_vuc', 'desc')->get();
        $tim_kiem_bds = DB::table('tbl_bat_dong_san')->where('tinh_trang_bds', '1')->where('ten_bds', 'like', '%'.$key_words.'%')->get();
        return view('pages.batdongsan.timkiem')
        ->with('loaibds', $danh_sach_the_loai)
        ->with('duan', $danh_sach_du_an)
        ->with('khuvuc', $danh_sach_khu_vuc)
        ->with('tim_kiem_bds',$tim_kiem_bds)->with('tim_kiem_bds', $tim_kiem_bds);
        
    }
}
