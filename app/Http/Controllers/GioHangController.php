<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
use Cart;
class GioHangController extends Controller
{
    //
    public function luu_gio_hang(Request $request)
    {
        
        // lay khi luu o gio hang
        $id_BDS = $request->id_BDS;
        $so_luong = $request->qty;
        //
        $danh_sach_the_loai=DB::table('tbl_theloai')->where('tinh_trang','1')->orderby('category_id', 'desc')->get();
        $danh_sach_du_an=DB::table('tbl_du_an')->where('tinh_trang_du_an', '1')->orderby('id_du_an', 'desc')->get();
        $danh_sach_khu_vuc=DB::table('tbl_khu_vuc')->where('tinh_trang_khu_vuc', '1')->orderby('id_khu_vuc', 'desc')->get();
        $danh_sach_bds=DB::table('tbl_bat_dong_san')->where('id_bds',$id_BDS)->first();
        //Cart::add('293ad', 'Product 1', 1, 9.99, 550);

        $data['id']= $danh_sach_bds->id_bds;
        $data['qty']= $so_luong;
        $data['name']= $danh_sach_bds->ten_bds;
        $data['price']= $danh_sach_bds->gia_bds;
        $data['weight']= $danh_sach_bds->dien_tich;
        $data['options']['image']= $danh_sach_bds->hinh_anh_bds;
        Cart::add($data);
        Cart::setGlobalTax(0);
        return Redirect::to('/hien-thi-gio-hang');
    }
    public function hien_thi_ds_gio_hang(){
        $danh_sach_the_loai=DB::table('tbl_theloai')->where('tinh_trang','1')->orderby('category_id', 'desc')->get();
        $danh_sach_du_an=DB::table('tbl_du_an')->where('tinh_trang_du_an', '1')->orderby('id_du_an', 'desc')->get();
        $danh_sach_khu_vuc=DB::table('tbl_khu_vuc')->where('tinh_trang_khu_vuc', '1')->orderby('id_khu_vuc', 'desc')->get();
       
        return view('pages.giohang.giohang')->with('loaibds', $danh_sach_the_loai)
        ->with('duan', $danh_sach_du_an)
        ->with('khuvuc', $danh_sach_khu_vuc);
    }
    public function xoa_gio_hang($rowId){
        Cart::update($rowId,0);
        return Redirect::to('/hien-thi-gio-hang');
    }
    public function cap_nhat_so_luong_cart(Request $request){
        $rowId = $request->rowID_cart;
        $qty= $request->cart_quantity;
        Cart::update($rowId,$qty);
        return Redirect::to('/hien-thi-gio-hang');
    }

    public function hien_thi_ds_gio_hang_ajax(Request $request){
        $danh_sach_the_loai=DB::table('tbl_theloai')->where('tinh_trang','1')->orderby('category_id', 'desc')->get();
        $danh_sach_du_an=DB::table('tbl_du_an')->where('tinh_trang_du_an', '1')->orderby('id_du_an', 'desc')->get();
        $danh_sach_khu_vuc=DB::table('tbl_khu_vuc')->where('tinh_trang_khu_vuc', '1')->orderby('id_khu_vuc', 'desc')->get();
       
        return view('pages.giohang.cart_ajax')->with('loaibds', $danh_sach_the_loai)
        ->with('duan', $danh_sach_du_an)
        ->with('khuvuc', $danh_sach_khu_vuc);
    }
    public function add_cart_ajax(Request $request){
        $data = $request->all();
        $session_id = substr(md5(microtime()),rand(0,26),5);
        $cart = Session::get('cart');
        if($cart==true){
            $is_avaiable = 0;
            foreach($cart as $key => $val){
                if($val['product_id']==$data['cart_product_id']){
                    $is_avaiable++;
                }
            }
            if($is_avaiable == 0){
                $cart[] = array(
                'session_id' => $session_id,
                'product_name' => $data['cart_product_name'],
                'product_id' => $data['cart_product_id'],
                'product_image' => $data['cart_product_image'],
                'product_qty' => $data['cart_product_qty'],
                'product_price' => $data['cart_product_price'],
                );
                Session::put('cart',$cart);
            }
        }else{
            $cart[] = array(
                'session_id' => $session_id,
                'product_name' => $data['cart_product_name'],
                'product_id' => $data['cart_product_id'],
                'product_image' => $data['cart_product_image'],
                'product_qty' => $data['cart_product_qty'],
                'product_price' => $data['cart_product_price'],

            );
            Session::put('cart',$cart);
        }
        Session::save();
    }  
    public function xoa_bds_gio_hang($session_id){
        $cart = Session::get('cart');
        
        if($cart==true){
            foreach($cart as $key => $val){
                if($val['session_id']==$session_id)
                {
                    unset($cart[$key]);
                }
            }
            Session::put('cart',$cart);
            return Redirect()->back()->with('message', 'Xóa thành công');
        }else{
            return Redirect()->back()->with('message', 'Xóa thất bại');
        }
       
    }
    public function cap_nhat_gio_hang(Request $request){
        $data = $request->all();
        $cart = Session::get('cart');
        if($cart==true)
        {
            foreach($data['cart_qty'] as $key => $qty){
                foreach($cart as $session => $val){
                    if($val['session_id']==$key){
                        $cart[$session]['product_qty'] = $qty;
                    }
                }
            }
            Session::put('cart', $cart);
            return Redirect()->back()->with('message', 'Cập nhật thành công');
        }
        else{
            return Redirect()->back()->with('message', 'Cập nhật thất bại');
        }
    }
    public function xoa_gio_hang_all(){
        $cart = Session::get('cart');
        if($cart==true)
        {
            Session::forget('cart');
            return Redirect()->back();
        }
    }


}
